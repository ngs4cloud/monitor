package utils;

import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 *
 */
public class IOUtils {

  public static class CouldNotWriteToStreamException extends RuntimeException{
    public CouldNotWriteToStreamException(String message)
    {
      super(message);
    }
  }

  public static void writeln(String str, OutputStreamWriter writer)
  {
    try {
      writer.write(str);
      writer.write('\n');
      writer.flush();
    } catch (IOException e) {
      throw new CouldNotWriteToStreamException(e.getMessage());
    }
  }
}
