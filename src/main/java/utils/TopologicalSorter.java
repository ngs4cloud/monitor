package utils;

import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.TopologicalOrderIterator;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.*;

public class TopologicalSorter {

  public static class KeysMustBeUniqueException extends RuntimeException{
  }

  public static class ThereAreCyclicalDependenciesException extends RuntimeException{
  }

  /**
   * Topologically sorts a stream of items.
   * @param items - The items to be topologically sorted.
   * @param extractKey - Extracts the key of the item. The key must be unique in the set of given items.
   * @param extractParents - Extracts the key of the items an item depends on.
   * @param <E> - Type of the items.
   * @param <T> - Type of the items' keys.
   * @return The given items topologically sorted.
   * @throws KeysMustBeUniqueException if there are items with the same key within the given set of items.
   * @throws ThereAreCyclicalDependenciesException if there are cyclic dependencies in the given items.
   */
  public static <E,T> Stream<E> sort(Stream<E> items, Function<E,T> extractKey, Function<E,Stream<T>> extractParents)
  {
    Map<T, E> keyItem =  items.collect(toMap(extractKey, Function.identity(), (i1,i2)->{throw new KeysMustBeUniqueException();}));

    DefaultDirectedGraph<E, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);
    keyItem.values().forEach(graph::addVertex);

    keyItem.values().forEach(i -> extractParents.apply(i).forEach(p -> graph.addEdge(keyItem.get(p), i)));

    throwIfThereAreCyclicalDependencies(graph);
    return getTopologicallySorted(keyItem, graph);
  }

  private static <E> void throwIfThereAreCyclicalDependencies(DefaultDirectedGraph<E, DefaultEdge> graph)
  {
    if(new CycleDetector<>(graph).detectCycles()){
      throw new ThereAreCyclicalDependenciesException();
    }
  }

  private static <E, T> Stream<E> getTopologicallySorted(Map<T, E> keyItem, DefaultDirectedGraph<E, DefaultEdge> graph)
  {
    TopologicalOrderIterator<E, DefaultEdge> orderIterator = new TopologicalOrderIterator<>(graph);
    return StreamSupport.stream(Spliterators.spliterator(orderIterator, keyItem.values().size(), Spliterator.ORDERED), false);
  }
}
