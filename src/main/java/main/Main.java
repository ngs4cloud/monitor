package main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.http.HttpClient;
import pipeline.ssh.SftpClient;

import java.util.stream.Collectors;


public class Main{

  public static final Logger logger = LogManager.getLogger(Main.class);

  public static void main(String[] arg)
  {
    try{
      logger.info("Starting execution");
      Program.make(System.out).Start(arg);
      logger.info("Finished execution");
    }catch (SftpClient.SftpChannel.SftpInteractionException e){
      System.out.println("An error has occurred when accessing the cluster, please verify the configurations " +
          "regarding the cluster are correct.");
      logger.error(e);
    }catch (SftpClient.AuthenticationFailedException e){
      System.out.println("The authentication on the cluster has failed, please verify your credentials.");
      logger.error(e);
    }catch (HttpClient.RequestFailedException e){
      System.out.println("An error has occurred when sending a request to Chronos, please verify the configurations " +
          "regarding Chronos are correct.");
      logger.error(e);
    }catch (ConfigsGetter.InvalidConfigsException e){
      System.out.println(e.getMessage());
      logger.error(e);
    }catch (Program.CouldNotAccessConfigurationsFileException e){
      System.out.println("Could not access the configurations.");
      logger.error(e);
    }catch (ConfigsGetter.NotAllRequiredConfigsAreDefinedException e){
      String undefConfigs = e.undefinedConfigs.stream()
          .map(ConfigsGetter.ConfigKey::toString)
          .collect(Collectors.joining(", "));
      System.out.println("The following configurations were not defined: " + undefConfigs);
      logger.error(e);
    }catch (Program.SpecifiedExecutionTrackerRepoIsInvalid e){
      System.out.println("The execution tracker repository is invalid. It either doesn't exist or is not a directory.");
      logger.error(e);
    }catch (Program.ConfigurationEnvVariableIsNotDefined e){
      System.out.println("The environmental variable " + e.envVarName + " needs to be defined with the path " +
          "of the configurations file.");
      logger.error(e);
    }catch (Program.GivenCommandArgumentsAreInvalidException e){
      System.out.println("The given combination of command and arguments is not valid. Type help to check all the available commands.");
      logger.error(e);
    }catch (Program.GivenPipelineExecutionIdIsInvalidException e){
      System.out.println("the given pipeline execution id is not valid.");
      logger.error(e);
    }catch (RuntimeException e){
      System.out.println("We're sorry, an error occurred.");
      logger.error(e);
    }
  }
}