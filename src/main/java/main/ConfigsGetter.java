package main;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

/**
 * Provides the configurations needed to execute the application.
 */
public class ConfigsGetter {

  /**
   * Enum containing all the possible configurable parameters.
   */
  public static enum ConfigKey {
    SSH_HOST(true), SSH_PORT(true), SSH_USER(true), SSH_PASS(false), SSH_PRIVATE_KEY_PATH(false),
    SSH_PRIVATE_KEY_PASS(false), SSH_PUBLIC_KEY_PATH(false), CHRONOS_HOST(true), CHRONOS_PORT(true),
    CLUSTER_SHARED_DIR_PATH(true), EXECUTION_TRACKER_REPO_PATH(true), PIPELINE_OWNER(true), WGET_DOCKER_IMAGE(true),
    P7ZIP_DOCKER_IMAGE(true);

    private final boolean isRequired;

    ConfigKey(boolean isRequired)
    {
      this.isRequired = isRequired;
    }

    public boolean isRequired()
    {
      return isRequired;
    }
  }

  public static class InvalidConfigsException extends RuntimeException{
    public InvalidConfigsException(String message)
    {
      super(message);
    }
  }

  public static class CouldNotAccessConfigurationsException extends RuntimeException{
    public CouldNotAccessConfigurationsException(String message)
    {
      super(message);
    }
  }

  public static class NotAllRequiredConfigsAreDefinedException extends RuntimeException{
    public final List<ConfigKey> undefinedConfigs;

    public NotAllRequiredConfigsAreDefinedException(List<ConfigKey> undefinedConfigs)
    {
      this.undefinedConfigs = undefinedConfigs;
    }
  }

  public static final Logger logger = LogManager.getLogger(ConfigsGetter.class);

  /**
   * Given an InputStream which contains the configurations as pairs
   * key/value, where the key and the values are separated by an equal sign,
   * and the pairs are separated by a new a line, returns an instance of ConfigsGetter.
   * The content of the InputStream should be something along these lines :
   *           KEY1=Value1
   *           Key2=Value2
   * @param is InputStream with the configurations.
   * @return ConfigsGetter with the configurations described in the InputStream.
   * @throws main.ConfigsGetter.NotAllRequiredConfigsAreDefinedException if a required
   *  configuration is not in the InputStream content.
   * @throws main.ConfigsGetter.InvalidConfigsException if the syntax of the InputStream content
   *  is not correct, or if there is a key not recognized.
   * @throws main.ConfigsGetter.CouldNotAccessConfigurationsException if the was a problem accessing
   *  the content of the InputStream.
   */
  public static ConfigsGetter make(InputStream is)
  {
    try {
      logger.info("Starting to get configurations");
      String configsStr = IOUtils.toString(is, "ISO-8859-1");
      Map<ConfigKey, String> configs = Arrays.stream(configsStr.split("\n"))
          .filter(s -> s.length() > 0 && !s.equals("\r"))
          .map(pair -> pair.split("="))
          .collect(toMap(pair -> mapToConfigKey(pair[0].trim()), pair -> pair[1].trim()));
      throwIfNotAllRequiredConfigsAreDefinedException(configs);
      ConfigsGetter configsGetter = new ConfigsGetter(configs);
      logger.info("Finished getting configurations");

      return configsGetter;
    }catch (IOException e){
      throw new CouldNotAccessConfigurationsException(e.getMessage());
    }catch (IndexOutOfBoundsException e){
      throw new InvalidConfigsException("The configuration file syntax is not correct");
    }
  }

  private static void throwIfNotAllRequiredConfigsAreDefinedException(Map<ConfigKey, String> configs)
  {
    List<ConfigKey> undefinedConfigs = Stream.of(ConfigKey.values())
        .filter(key -> key.isRequired && !configs.containsKey(key))
        .collect(toList());

    if(!undefinedConfigs.isEmpty()){
      throw new NotAllRequiredConfigsAreDefinedException(undefinedConfigs);
    }
  }

  private static ConfigKey mapToConfigKey(String key)
  {
    try{
      return ConfigKey.valueOf(key);
    }catch (IllegalArgumentException e){
      throw new InvalidConfigsException("There is no " + key + " configuration");
    }
  }

  private final Map<ConfigKey, String> configs;

  private ConfigsGetter(Map<ConfigKey, String> configs)
  {
    this.configs = configs;
  }

  public String getSshHost()
  {
    return configs.get(ConfigKey.SSH_HOST);
  }

  public int getSshPort()
  {
    return Integer.parseInt(configs.get(ConfigKey.SSH_PORT));
  }

  public String getSshUser()
  {
    return configs.get(ConfigKey.SSH_USER);
  }

  public Optional<String> getSshPass()
  {
    return Optional.ofNullable(configs.get(ConfigKey.SSH_PASS));
  }

  public Optional<String> getSshPrivateKeyPath()
  {
    return Optional.ofNullable(configs.get(ConfigKey.SSH_PRIVATE_KEY_PATH));
  }

  public Optional<String> getSshPrivateKeyPass()
  {
    return Optional.ofNullable(configs.get(ConfigKey.SSH_PRIVATE_KEY_PASS)) ;
  }

  public Optional<String> getSshPublicKeyPath()
  {
    return Optional.ofNullable(configs.get(ConfigKey.SSH_PUBLIC_KEY_PATH));
  }

  public String getChronosHost()
  {
    return configs.get(ConfigKey.CHRONOS_HOST);
  }

  public int getChronosPort()
  {
    return Integer.parseInt(configs.get(ConfigKey.CHRONOS_PORT));
  }

  public String getClusterSharedDirPath()
  {
    return configs.get(ConfigKey.CLUSTER_SHARED_DIR_PATH);
  }

  public String getExecutionTrackerRepoPath()
  {
    return configs.get(ConfigKey.EXECUTION_TRACKER_REPO_PATH);
  }

  public String getPipelineOwner()
  {
    return configs.get(ConfigKey.PIPELINE_OWNER);
  }

  public String getWgetDockerImage()
  {
    return configs.get(ConfigKey.WGET_DOCKER_IMAGE);
  }

  public String getP7zipDockerImage()
  {
    return configs.get(ConfigKey.P7ZIP_DOCKER_IMAGE);
  }
}
