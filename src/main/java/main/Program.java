package main;

import main.command.LaunchCommand;
import main.command.OutputsCommand;
import main.command.StatusCommand;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.execution.Path;
import pipeline.ssh.JSchSftpClientAdapter;
import pipeline.ssh.ProgressMonitor;
import pipeline.ssh.SftpClient;

import java.io.*;
import java.util.Optional;

import static utils.IOUtils.writeln;

/**
 *
 */
public class Program{

  public static class SpecifiedExecutionTrackerRepoIsInvalid extends RuntimeException{
  }

  public static class ConfigurationEnvVariableIsNotDefined extends RuntimeException
  {
    public final String envVarName;

    public ConfigurationEnvVariableIsNotDefined(String envVarName)
    {
      this.envVarName = envVarName;
    }
  }

  public static class CouldNotAccessConfigurationsFileException extends RuntimeException{
    public CouldNotAccessConfigurationsFileException(String message)
    {
      super(message);
    }
  }

  public static class GivenFilesForSshPublicKeyAuthenticationAreInvalidException extends RuntimeException{
    public GivenFilesForSshPublicKeyAuthenticationAreInvalidException(String message)
    {
      super(message);
    }
  }

  public static class GivenCommandArgumentsAreInvalidException extends RuntimeException{

  }

  public static class GivenPipelineExecutionIdIsInvalidException extends RuntimeException{

  }

  public static final Logger logger = LogManager.getLogger(Program.class);

  public static Program make(OutputStream out)
  {
    final String configEnvVar = "NGS4_CLOUD_MONITOR_CONFIGS";
    String configsPath = System.getenv(configEnvVar);

    if(configsPath == null){
      throw new ConfigurationEnvVariableIsNotDefined(configEnvVar);
    }

    try{
      ConfigsGetter configsGetter = ConfigsGetter.make(new FileInputStream(configsPath));
      return new Program(configsGetter, out);
    } catch (FileNotFoundException e) {
      throw new CouldNotAccessConfigurationsFileException(e.getMessage());
    }
  }

  private final ConfigsGetter configsGetter;
  private final OutputStreamWriter outWriter;

  public Program(ConfigsGetter configsGetter, OutputStream out)
  {
    this.configsGetter = configsGetter;
    this.outWriter = new OutputStreamWriter(out);
  }

  public void Start(String[] args)
  {
    throwIfSpecifiedExecutionTrackerRepoDoesNotExistOrIsNotADirectory();
    String command = args.length > 0 ? args[0] : "";
    logger.info("Executing command: " + command);
    switch (command){
      case "launch": launchPipeline(args); break;
      case "status": checkPipelineStatus(args); break;
      case "outputs": downloadOutputs(args); break;
      default: printHelpMessage();
    }
  }

  private void launchPipeline(String [] args)
  {
    if (args.length != 2){
      throw new GivenCommandArgumentsAreInvalidException();
    }
    String irPath = args[1];
    new LaunchCommand(configsGetter, makeSftpClient(), System.out).run(irPath);
  }

  private void checkPipelineStatus(String[] args)
  {
    if(args.length != 2){
      throw new GivenCommandArgumentsAreInvalidException();
    }
    String pipelineId = args[1];
    new StatusCommand(configsGetter, System.out).run(parsePipelineId(pipelineId));
  }

  private void downloadOutputs(String [] args)
  {
    if(args.length != 3){
      throw new GivenCommandArgumentsAreInvalidException();
    }
    String pipelineId = args[1];
    String destination = args[2];
    new OutputsCommand(configsGetter, makeSftpClient(), System.out).run(parsePipelineId(pipelineId), Path.make(destination));
  }

  private void printHelpMessage()
  {
    writeln("Commands:\n" +
        "\tlaunch {IrPath} - launches pipeline execution.\n" +
        "\tstatus {PipelineId} - reports status of execution.\n" +
        "\toutputs {PipelineId} {DestPath} - Downloads outputs.\n", outWriter);
  }

  private SftpClient makeSftpClient()
  {
    String user = configsGetter.getSshUser();
    String host = configsGetter.getSshHost();
    int port = configsGetter.getSshPort();

    ProgressMonitor progressMonitor = new ProgressMonitor(System.out);
    Optional<String> pass = configsGetter.getSshPass();
    if(pass.isPresent()){
      return JSchSftpClientAdapter.makePasswordAuthenticationSftpClient(user, host ,port ,pass.get(), progressMonitor);
    }
    try {
      String keyPairName = "name";
      byte[] pubKey = IOUtils.toByteArray(new FileInputStream(new File(configsGetter.getSshPublicKeyPath().get())));
      byte[] privKey = IOUtils.toByteArray(new FileInputStream(new File(configsGetter.getSshPrivateKeyPath().get())));
      byte[] privKeyPass = configsGetter.getSshPrivateKeyPass().get().getBytes();
      return JSchSftpClientAdapter.makePublicKeyAuthenticationSftpClient(user, host, port, keyPairName,
          pubKey, privKey, privKeyPass, progressMonitor);
    }catch (IOException e){
      throw new GivenFilesForSshPublicKeyAuthenticationAreInvalidException(e.getMessage());
    }
  }

  private void throwIfSpecifiedExecutionTrackerRepoDoesNotExistOrIsNotADirectory()
  {
    File repo = new File(configsGetter.getExecutionTrackerRepoPath());
    if(!repo.exists() || !repo.isDirectory()){
      throw new SpecifiedExecutionTrackerRepoIsInvalid();
    }
  }

  private long parsePipelineId(String pipelineId)
  {
    try {
      return Long.parseLong(pipelineId);
    }catch (NumberFormatException e){
      throw new GivenPipelineExecutionIdIsInvalidException();
    }
  }
}
