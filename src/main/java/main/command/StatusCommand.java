package main.command;

import main.ConfigsGetter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.http.UnirestAdapter;
import pipeline.repo.FlatFileExecutionTrackerRepo;
import pipeline.status.ExecutionStatusGetter;
import pipeline.status.chronos.ChronosJobsStatusGetter;

import java.io.OutputStream;
import java.io.OutputStreamWriter;

import static utils.IOUtils.writeln;

/**
 * Displays the status of the execution of a pipeline.
 */
public class StatusCommand {

  public static final Logger logger = LogManager.getLogger(StatusCommand.class);

  private final ConfigsGetter configsGetter;
  private final OutputStreamWriter outWriter;

  public StatusCommand(ConfigsGetter configsGetter, OutputStream out)
  {
    this.configsGetter = configsGetter;
    this.outWriter = new OutputStreamWriter(out);
  }

  /**
   * Displays the status of the execution of a pipeline with a given id.
   * @param pipelineId - Id of the pipeline.
   */
  public void run(long pipelineId)
  {
    try {
      internalRun(pipelineId);
    } catch (FlatFileExecutionTrackerRepo.NoSuchPipelineExecutionException e){
      writeln("There is no pipeline execution associated with the given id.", outWriter);
      logger.error(e);
    } catch (ChronosJobsStatusGetter.CouldNotGetChronosJobStatusException e){
      writeln("Could not get the status of the pipeline tasks, please verify the configurations " +
          "regarding Chronos are correct.", outWriter);
      logger.error(e);
    }
  }

  private void internalRun(long pipelineId)
  {
    FlatFileExecutionTrackerRepo repo = new FlatFileExecutionTrackerRepo(configsGetter.getExecutionTrackerRepoPath());
    ChronosJobsStatusGetter jobsStatusGetter = new ChronosJobsStatusGetter(configsGetter.getChronosHost(),
        configsGetter.getChronosPort(),
        new UnirestAdapter());
    ExecutionStatusGetter executionStatusGetter = new ExecutionStatusGetter(() -> repo.get(pipelineId),
        jobsStatusGetter::getJobsStatus);
    writeln(executionStatusGetter.get().getMessage(), outWriter);
  }
}
