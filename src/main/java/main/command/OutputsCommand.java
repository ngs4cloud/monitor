package main.command;

import main.ConfigsGetter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.execution.Path;
import pipeline.http.UnirestAdapter;
import pipeline.output.OutputsDownloader;
import pipeline.output.OutputEngine;
import pipeline.repo.ExecutionTracker;
import pipeline.repo.FlatFileExecutionTrackerRepo;
import pipeline.ssh.SftpClient;
import pipeline.status.ExecutionStatusGetter;
import pipeline.status.chronos.ChronosJobsStatusGetter;

import java.io.OutputStream;
import java.io.OutputStreamWriter;

import static utils.IOUtils.writeln;

/**
 * Downloads the outputs of a pipeline.
 */
public class OutputsCommand {

  public static final Logger logger = LogManager.getLogger(OutputsCommand.class);

  private final ConfigsGetter configsGetter;
  private final SftpClient sftpClient;
  private final OutputStreamWriter outWriter;

  public OutputsCommand(ConfigsGetter configsGetter, SftpClient sftpClient, OutputStream out)
  {
    this.configsGetter = configsGetter;
    this.sftpClient = sftpClient;
    this.outWriter = new OutputStreamWriter(out);
  }

  /**
   * Downloads the outputs of a pipeline with a given id to a given destination.
   * @param pipelineId - Id of the pipeline.
   * @param outputDestination - Location to where the pipeline outputs will be downloaded.
   */
  public void run(long pipelineId, Path outputDestination)
  {
    try {
      internalRun(pipelineId, outputDestination);
    } catch (FlatFileExecutionTrackerRepo.NoSuchPipelineExecutionException e) {
      writeln("There is no pipeline execution associated with the given id.", outWriter);
      logger.error(e);
    } catch (ChronosJobsStatusGetter.CouldNotGetChronosJobStatusException e) {
      writeln("Could not get the status of the pipeline tasks, please verify the configurations " +
          "regarding Chronos are correct.", outWriter);
      logger.error(e);
    } catch (OutputEngine.GivenDestDirDoesNotExistsException e){
      writeln("The directory given to download the pipeline outputs to, doesn't exist.", outWriter);
      logger.error(e);
    } catch (OutputEngine.PipelineExecutionHasFailedException e){
      writeln("Could not download the pipeline outputs because the pipeline execution has failed.", outWriter);
      logger.error(e);
    } catch (OutputEngine.PipelineExecutionStillInProgressException e){
      writeln("Could not download the pipeline outputs because the pipeline execution is still in progress.", outWriter);
      logger.error(e);
    }
  }

  private void internalRun(long pipelineId, Path outputDestination)
  {
    FlatFileExecutionTrackerRepo repo = new FlatFileExecutionTrackerRepo(configsGetter.getExecutionTrackerRepoPath());
    ExecutionTracker tracker = repo.get(pipelineId);

    ChronosJobsStatusGetter jobsStatusGetter = new ChronosJobsStatusGetter(configsGetter.getChronosHost(),
        configsGetter.getChronosPort(),
        new UnirestAdapter());
    ExecutionStatusGetter executionStatusGetter = new ExecutionStatusGetter(() -> tracker, jobsStatusGetter::getJobsStatus);

    OutputsDownloader outputsDownloader = new OutputsDownloader(sftpClient, tracker.getWorkDir());
    OutputEngine engine = new OutputEngine(executionStatusGetter::get,
        ()->tracker.getOutputs().stream()
        , outputsDownloader::download);

    engine.run(outputDestination);
  }
}
