package main.command;

import main.ConfigsGetter;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.execution.Path;
import pipeline.execution.parser.ExecutionParser;
import pipeline.http.HttpClient;
import pipeline.http.UnirestAdapter;
import pipeline.launch.ClusterInitializer;
import pipeline.launch.LaunchEngine;
import pipeline.launch.chronos.ImpChronosJobsLauncher;
import pipeline.launch.chronos.ImpChronosJobsFactory;
import pipeline.repo.FlatFileExecutionTrackerRepo;
import pipeline.ssh.SftpClient;
import utils.TopologicalSorter;

import java.io.*;

import static utils.IOUtils.writeln;

/**
 * Launches the execution of a pipeline.
 */
public class LaunchCommand {

  public static final Logger logger = LogManager.getLogger(LaunchCommand.class);

  public static class SpecifiedIrFileDoesNotExistsException extends RuntimeException{
    public SpecifiedIrFileDoesNotExistsException(String message)
    {
      super(message);
    }
  }

  private final ConfigsGetter configsGetter;
  private final SftpClient sftpClient;
  private final OutputStreamWriter outWriter;

  public LaunchCommand(ConfigsGetter configsGetter, SftpClient sftpClient, OutputStream out)
  {
    this.configsGetter = configsGetter;
    this.sftpClient = sftpClient;
    this.outWriter = new OutputStreamWriter(out);
  }

  /**
   * Launches the execution of a pipeline which is
   * described using the intermediate representation.
   * @param irPath - String which contains the path to a file
   *               with the intermediate representation of a pipeline.
   */
  public void run(String irPath)
  {
    try{
      internalRun(irPath);
    }catch (SftpClient.SftpChannel.FileGivenAsInputDoesNotExistException e){
      writeln("The specified input files for the pipeline's execution do not exist.", outWriter);
      logger.error(e);
    }catch(ExecutionParser.DoesNotFollowIRSchemaException e){
      writeln("The given IR file does not follow the schema.", outWriter);
      logger.error(e);
    }catch (ExecutionParser.InvalidIRException e){
      writeln("The given IR file is not a valid json file.", outWriter);
      logger.error(e);
    }catch (ImpChronosJobsLauncher.JobLaunchFailedException e){
      writeln("Could not launch the pipeline tasks, please verify the configurations " +
          "regarding Chronos are correct.", outWriter);
      logger.error(e);
    }catch (FlatFileExecutionTrackerRepo.CouldNotAccessFileException e){
      writeln("Could not store the information regarding the pipeline execution. please verify the configuration" +
          " regarding the executors tracker repository.", outWriter);
      logger.error(e);
    }catch (TopologicalSorter.KeysMustBeUniqueException e){
      writeln("There are tasks with the same id.", outWriter);
      logger.error(e);
    }catch (TopologicalSorter.ThereAreCyclicalDependenciesException e){
      writeln("There are cyclical dependencies between the tasks.", outWriter);
      logger.error(e);
    }catch (SpecifiedIrFileDoesNotExistsException e){
      writeln("The specified IR file doesn't exists.", outWriter);
      logger.error(e);
    }
  }

  private void internalRun(String irPath)
  {
    ExecutionParser execParser = new ExecutionParser();

    Path sharedDir = Path.make(configsGetter.getClusterSharedDirPath());
    Path pipelineWorkDir = sharedDir.concat(Path.make(RandomStringUtils.randomAlphabetic(12)));
    ClusterInitializer clusterInitializer = new ClusterInitializer(sftpClient);

    String baseJobName = RandomStringUtils.randomAlphabetic(12);
    HttpClient httpClient = new UnirestAdapter();
    ImpChronosJobsFactory jobsFactory = new ImpChronosJobsFactory(pipelineWorkDir, baseJobName, configsGetter.getPipelineOwner(),
        configsGetter.getChronosHost(), configsGetter.getChronosPort(),
        configsGetter.getWgetDockerImage(), configsGetter.getP7zipDockerImage());
    ImpChronosJobsLauncher jobLauncher = new ImpChronosJobsLauncher(
        configsGetter.getChronosHost(),
        configsGetter.getChronosPort(),
        jobsFactory,
        httpClient
    );
    FlatFileExecutionTrackerRepo repo = new FlatFileExecutionTrackerRepo(configsGetter.getExecutionTrackerRepoPath());
    LaunchEngine engine = new LaunchEngine(execParser::parse, clusterInitializer::initCluster, jobLauncher,
        repo::create, pipelineWorkDir);

    String executionId = Long.toString(engine.run(getIrInputStream(irPath)));
    logger.info("Execution with id: " + executionId + " has been launched");
    writeln("Id: " + executionId, outWriter);
  }

  private FileInputStream getIrInputStream(String irPath)
  {
    try {
      return new FileInputStream(new File(irPath));
    } catch (FileNotFoundException e) {
      throw new SpecifiedIrFileDoesNotExistsException("The file " + irPath + "doesn't exists" );
    }
  }
}
