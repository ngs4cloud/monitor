package pipeline.status.chronos;

/**
 * Status of a Chronos job.
 */
public class ChronosJobStatus {

  private final String name;
  private final int successCount;
  private final int errorCount;
  private final int retries;

  public ChronosJobStatus(String name, int successCount, int errorCount, int retries)
  {
    this.name = name;
    this.successCount = successCount;
    this.errorCount = errorCount;
    this.retries = retries;
  }

  public String getName()
  {
    return this.name;
  }

  public int getSuccessCount()
  {
    return this.successCount;
  }

  public int getErrorCount()
  {
    return this.errorCount;
  }

  public int getRetries()
  {
    return this.retries;
  }

  public boolean hasFinished()
  {
    return this.successCount > 0;
  }

  public boolean hasFailed()
  {
    return this.errorCount > 0;
  }

  public boolean isInProgress()
  {
    return this.successCount == 0 && this.errorCount == 0;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ChronosJobStatus that = (ChronosJobStatus) o;

    if (errorCount != that.errorCount) return false;
    if (retries != that.retries) return false;
    if (successCount != that.successCount) return false;
    if (!name.equals(that.name)) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result = name.hashCode();
    result = 31 * result + successCount;
    result = 31 * result + errorCount;
    result = 31 * result + retries;
    return result;
  }
}
