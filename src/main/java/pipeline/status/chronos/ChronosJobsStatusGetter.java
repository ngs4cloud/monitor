package pipeline.status.chronos;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.http.HttpClient;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

/**
 * Gets the status of all Chronos jobs.
 */
public class ChronosJobsStatusGetter {

  public static class CouldNotGetChronosJobStatusException extends RuntimeException{
    public CouldNotGetChronosJobStatusException(String message)
    {
      super(message);
    }
  }

  public static final Logger logger = LogManager.getLogger(ChronosJobsStatusGetter.class);

  private final String chronosHost;
  private final int chronosPort;
  private final HttpClient httpClient;

  public ChronosJobsStatusGetter(String chronosHost, int chronosPort, HttpClient httpClient)
  {
    this.chronosHost = chronosHost;
    this.chronosPort = chronosPort;
    this.httpClient = httpClient;
  }

  /**
   * Gets the status of all Chronos jobs.
   * @return Stream of the status of the Chronos jobs.
   */
  public Stream<ChronosJobStatus> getJobsStatus()
  {
    logger.info("Starting to get status of launched Chronos jobs.");

    final String listJobsEndpoint = "http://" + chronosHost + ":" + chronosPort + "/scheduler/jobs";
    HttpClient.HttpResponse<List<ChronosJobStatusDto>> res =
        httpClient.getAsJson(listJobsEndpoint, new TypeReference<List<ChronosJobStatusDto>>(){});
    throwIfInvalidHttpStatus(res.getStatus());
    Stream<ChronosJobStatus> jobs = res.getBody().stream().map(this::getJobStatus);

    logger.info("Finished getting status of launched Chronos jobs.");
    return jobs;
  }

  private void throwIfInvalidHttpStatus(int status)
  {
    if (status < 200 || 299 < status){
      throw new CouldNotGetChronosJobStatusException("Request failed");
    }
  }

  private ChronosJobStatus getJobStatus(ChronosJobStatusDto dto)
  {
    return new ChronosJobStatus(dto.name, dto.successCount, dto.errorCount, dto.retries);
  }
}
