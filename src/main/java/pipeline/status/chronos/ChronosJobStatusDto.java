package pipeline.status.chronos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO of a Chronos job status.
 */
public class ChronosJobStatusDto {

  public final String name;
  public final int successCount;
  public final int errorCount;
  public final int retries;

  @JsonCreator
  public ChronosJobStatusDto(@JsonProperty("name") String name,
                             @JsonProperty("successCount") int successCount,
                             @JsonProperty("errorCount") int errorCount,
                             @JsonProperty("retries") int retries)
  {
    this.name = name;
    this.successCount = successCount;
    this.errorCount = errorCount;
    this.retries = retries;
  }
}
