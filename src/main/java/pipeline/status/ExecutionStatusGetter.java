package pipeline.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.repo.ExecutionTracker;
import pipeline.status.chronos.ChronosJobStatus;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

/**
 * Gets the execution status of a pipeline.
 */
public class ExecutionStatusGetter {

  public static class NotAllPipelineJobsAreStagedException extends RuntimeException{
  }

  public static final Logger logger = LogManager.getLogger(ExecutionStatusGetter.class);

  private final Supplier<ExecutionTracker> executionTrackerGetter;
  private final Supplier<Stream<ChronosJobStatus>> jobsStatusGetter;

  /**
   * @param executionTrackerGetter - Supplier of the Execution tracker of the pipeline
   *                               which status will be determined.
   * @param jobsStatusGetter - Supplier which gets the status of all the Chronos jobs.
   */
  public ExecutionStatusGetter(Supplier<ExecutionTracker> executionTrackerGetter,
                               Supplier<Stream<ChronosJobStatus>> jobsStatusGetter)
  {
    this.executionTrackerGetter = executionTrackerGetter;
    this.jobsStatusGetter = jobsStatusGetter;
  }

  public ExecutionStatus get()
  {
    logger.info("Getting execution status");
    ExecutionTracker tracker = executionTrackerGetter.get();
    Set<String> jobNames = new HashSet<>(tracker.getJobNames());
    List<ChronosJobStatus> jobsOfExecution = jobsStatusGetter.get()
        .filter(j -> jobNames.contains(j.getName()))
        .collect(toList());

    if(tracker.getJobNames().size() != jobsOfExecution.size()){
      throw new NotAllPipelineJobsAreStagedException();
    }

    ExecutionStatus executionStatus = getExecutionStatus(jobsOfExecution);
    logger.info("Got execution status");
    return executionStatus;
  }

  private ExecutionStatus getExecutionStatus(List<ChronosJobStatus> jobsStatus)
  {
    long njobs = jobsStatus.size();
    long completedJobs = 0L;
    for (ChronosJobStatus job : jobsStatus) {
      if(job.hasFinished()){
        completedJobs++;
      }else {
        if (job.hasFailed()) {
          return ExecutionStatus.makeFailed("The pipeline execution has failed.");
        }
      }
    }

    return njobs == completedJobs ?
        ExecutionStatus.makeFinished("The pipeline execution has finished with success."):
        ExecutionStatus.makeInProgress("In progress, " + completedJobs + "/" + njobs + " jobs have finished.");
  }
}
