package pipeline.status;

/**
 * Execution status of a pipeline.
 */
public class ExecutionStatus {

  public static ExecutionStatus makeFailed(String message)
  {
    return new ExecutionStatus(Type.FAILED, message);
  }

  public static ExecutionStatus makeInProgress(String message)
  {
    return new ExecutionStatus(Type.IN_PROGRESS, message);
  }
  public static ExecutionStatus makeFinished(String message)
  {
    return new ExecutionStatus(Type.FINISHED, message);
  }

  private static enum Type{
    FINISHED, IN_PROGRESS, FAILED
  }

  private final Type type;
  private final String message;

  private ExecutionStatus(Type type, String message)
  {
    this.type = type;
    this.message = message;
  }

  public String getMessage()
  {
    return message;
  }

  public boolean hasFinished()
  {
    return type == Type.FINISHED;
  }

  public boolean hasFailed()
  {
    return type == Type.FAILED;
  }

  public boolean isInProgress()
  {
    return type == Type.IN_PROGRESS;
  }
}
