package pipeline.execution.parser;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import pipeline.execution.Task;

import java.util.List;

/**
 * DTO of the Intermediate representation.
 */
public class IRDto {

  public final List<String> directories;
  public final List<String> outputs;
  public final List<Task> tasks;
  public final String input;

  @JsonCreator
  public IRDto(@JsonProperty("directories")List<String> directories,
               @JsonProperty("ouputs")List<String> outputs,
               @JsonProperty("tasks")List<Task> tasks,
               @JsonProperty("input")String input)
  {
    this.directories = directories;
    this.outputs = outputs;
    this.tasks = tasks;
    this.input = input;
  }
}
