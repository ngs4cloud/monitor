package pipeline.execution.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.execution.Execution;
import pipeline.execution.Path;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.stream.Collectors;

public class ExecutionParser {

  public static class DoesNotFollowIRSchemaException extends RuntimeException{
    public DoesNotFollowIRSchemaException(String message)
    {
      super(message);
    }
  }

  public static class InvalidIRException extends RuntimeException{
    public InvalidIRException(String message)
    {
      super(message);
    }
  }

  public static final Logger logger = LogManager.getLogger(ExecutionParser.class);

  /**
   * Parses an Intermediate representation as an execution.
   * @param intermediateRepresentation - InputStream containing an
   *                                   Intermediate representation.
   * @return an Execution which describes the execution of the pipeline
   * as it was specified in the given Intermediate representation
   */
  public Execution parse(InputStream intermediateRepresentation)
  {
    try {
      logger.info("Starting to parse Execution from IR file.");
      StringWriter writer = new StringWriter();
      IOUtils.copy(intermediateRepresentation, writer, "ISO-8859-1");
      String irJson = writer.toString();
      throwIfInvalid(irJson);

      IRDto dto = new ObjectMapper().readValue(irJson, IRDto.class);
      logger.info("Finished parsing Execution from IR file.");
      return map(dto);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private Execution map(IRDto dto)
  {
    return new Execution(
        dto.directories.stream().map(Path::make).collect(Collectors.toList()),
        getInputUri(dto.input),
        dto.tasks,
        dto.outputs.stream().map(Path::make).collect(Collectors.toList())
        );
  }

  private URI getInputUri(String inputUri)
  {
    try {
      return new URI(inputUri);
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
  }

  private void throwIfInvalid(String irJson)
  {
    try {
      final String JSON_V4_SCHEMA_IDENTIFIER = "http://json-schema.org/draft-04/schema#";
      final String JSON_SCHEMA_IDENTIFIER_ELEMENT = "$schema";

      final JsonNode schemaNode = JsonLoader.fromResource("/IRJsonSchema.json");
      final JsonNode schemaIdentifier = schemaNode.get(JSON_SCHEMA_IDENTIFIER_ELEMENT);
      if (null == schemaIdentifier) {
        ((ObjectNode) schemaNode).put(JSON_SCHEMA_IDENTIFIER_ELEMENT, JSON_V4_SCHEMA_IDENTIFIER);
      }
      final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
      JsonSchema jsonSchema = factory.getJsonSchema(schemaNode);
      JsonNode dataNode = JsonLoader.fromString(irJson);
      ProcessingReport report = jsonSchema.validate(dataNode);

      if (!report.isSuccess()) {
        throw new DoesNotFollowIRSchemaException("invalid IR:\n" + irJson);
      }
    }catch (IOException|ProcessingException e){
      throw new InvalidIRException(e.getMessage());
    }
  }
}
