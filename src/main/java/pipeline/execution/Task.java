package pipeline.execution;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Data structure which describes a task of the pipeline.
 */
public class Task {

  private final int id;
  private final String dockerImage;
  private final String command;
  private final int mem;
  private final int disk;
  private final float cpus;
  private final List<Integer> parents;

  /**
   * @param id - Id of the task.
   * @param dockerImage - Docker image containing the tool which will be executed.
   * @param command - Command line command which will execute a tool.
   * @param mem - Memory needed to execute the task.
   * @param disk - Disk needed to execute the task.
   * @param cpus - Cpus needed to execute the task.
   * @param parents - List of tasks which need have been successfully executed in order
   *                for this task to be able to execute with success.
   */
  @JsonCreator
  public Task(@JsonProperty("id")int id,
              @JsonProperty("dockerImage")String dockerImage,
              @JsonProperty("command")String command,
              @JsonProperty("mem")int mem,
              @JsonProperty("disk")int disk,
              @JsonProperty("cpus")float cpus,
              @JsonProperty("parents")List<Integer> parents)
  {
    this.id = id;
    this.dockerImage = dockerImage;
    this.command = command;
    this.mem = mem;
    this.disk = disk;
    this.cpus = cpus;
    this.parents = parents;
  }

  public int getId()
  {
    return id;
  }

  public String getDockerImage()
  {
    return dockerImage;
  }

  public String getCommand()
  {
    return command;
  }

  public int getMem()
  {
    return mem;
  }

  public int getDisk()
  {
    return disk;
  }

  public float getCpus()
  {
    return cpus;
  }

  public List<Integer> getParents()
  {
    return parents;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Task task = (Task) o;

    if (Float.compare(task.cpus, cpus) != 0) return false;
    if (disk != task.disk) return false;
    if (id != task.id) return false;
    if (mem != task.mem) return false;
    if (!command.equals(task.command)) return false;
    if (!dockerImage.equals(task.dockerImage)) return false;
    if (!parents.equals(task.parents)) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result = id;
    result = 31 * result + dockerImage.hashCode();
    result = 31 * result + command.hashCode();
    result = 31 * result + mem;
    result = 31 * result + disk;
    result = 31 * result + (cpus != +0.0f ? Float.floatToIntBits(cpus) : 0);
    result = 31 * result + parents.hashCode();
    return result;
  }
}
