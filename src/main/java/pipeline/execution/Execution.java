package pipeline.execution;


import java.net.URI;
import java.util.List;

/**
 * Data structure which contains data regarding the execution of
 * a pipeline.
 */
public class Execution {

  private final List<Path> directories;
  private final URI input;
  private final List<Task> tasks;
  private final List<Path> outputs;

  /**
   *
   * @param directories - List of the directories that need to be created for the pipeline execution.
   * @param input - URI of pipeline's input.
   * @param tasks - List of the tasks that compose the pipeline.
   * @param outputs - Lis of the paths to the outputs which will be produced when the pipeline execution has finished.
   */
  public Execution(List<Path> directories, URI input, List<Task> tasks, List<Path> outputs)
  {
    this.directories = directories;
    this.input = input;
    this.tasks = tasks;
    this.outputs = outputs;
  }

  public List<Path> getDirectories()
  {
    return directories;
  }

  public URI getInput()
  {
    return input;
  }

  public List<Task> getTasks()
  {
    return tasks;
  }

  public List<Path> getOutputs()
  {
    return outputs;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Execution execution = (Execution) o;

    if (!directories.equals(execution.directories)) return false;
    if (!input.equals(execution.input)) return false;
    if (!outputs.equals(execution.outputs)) return false;
    if (!tasks.equals(execution.tasks)) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result = directories.hashCode();
    result = 31 * result + input.hashCode();
    result = 31 * result + tasks.hashCode();
    result = 31 * result + outputs.hashCode();
    return result;
  }
}
