package pipeline.execution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.*;

/**
 *
 */
public class Path {

  public static class InvalidPathException extends RuntimeException {
    public InvalidPathException(String message)
    {
      super(message);
    }
  }

  public static Path make(String strPath)
  {
    if(strPath.isEmpty()){
      throw new InvalidPathException("Path can't be empty");
    }

    boolean isAbsolute = strPath.startsWith("/");
    boolean isShortFormRelative = strPath.startsWith("./");
    List<String> segments = Arrays.stream(strPath.split("/")).collect(toList());
    if (isAbsolute || isShortFormRelative){
      segments.remove(0);
    }
    return new Path(segments, isAbsolute);
  }

  private final List<String> segments;
  private final boolean isAbsolute;

  protected Path(List<String> segments, boolean isAbsolute)
  {
    this.segments = segments;
    this.isAbsolute = isAbsolute;
  }

  public boolean isAbsolute()
  {
    return isAbsolute;
  }

  public int getDepth()
  {
    return segments.size()-1;
  }

  public Path concat(Path toAppend)
  {
    List<String> newSegments = new ArrayList<>(segments);
    newSegments.addAll(toAppend.segments);
    return new Path(newSegments, isAbsolute());
  }

  public Path getLastSegment()
  {
    return Path.make(segments.get(segments.size() - 1));
  }

  @Override
  public String toString()
  {
    String path = segments.stream().collect(joining("/"));
    return isAbsolute ? "/" + path : path;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Path path = (Path) o;

    if (!segments.equals(path.segments)) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    return segments.hashCode();
  }
}
