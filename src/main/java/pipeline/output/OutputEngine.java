package pipeline.output;

import pipeline.execution.Path;
import pipeline.status.ExecutionStatus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 *
 */
public class OutputEngine {

  private final Supplier<ExecutionStatus> executionStatusGetter;
  private final Supplier<Stream<Path>> outputPathsGetter;
  private final Consumer<Stream<Output>> outputDownloader;

  public static class PipelineExecutionHasFailedException extends RuntimeException {
    public PipelineExecutionHasFailedException(String message)
    {
      super(message);
    }
  }

  public static class PipelineExecutionStillInProgressException extends RuntimeException {
    public PipelineExecutionStillInProgressException(String message)
    {
      super(message);
    }
  }

  public static class GivenDestDirDoesNotExistsException extends RuntimeException{
    public GivenDestDirDoesNotExistsException(String message)
    {
      super(message);
    }
  }

  /**
   * @param executionStatusGetter - Gets the ExecutionStatus of the pipeline.
   * @param outputPathsGetter - Gets a Stream of the outputs produced by the pipeline.
   * @param outputDownloader - Downloads the given Outputs.
   */
  public OutputEngine(Supplier<ExecutionStatus> executionStatusGetter,
                      Supplier<Stream<Path>> outputPathsGetter,
                      Consumer<Stream<Output>> outputDownloader)
  {
    this.executionStatusGetter = executionStatusGetter;
    this.outputPathsGetter = outputPathsGetter;
    this.outputDownloader = outputDownloader;
  }

  public void run(Path destDir)
  {
    final ExecutionStatus executionStatus = executionStatusGetter.get();
    if (!executionStatus.hasFinished()) {
      throw executionStatus.hasFailed() ?
          new PipelineExecutionHasFailedException("The pipeline execution has failed") :
          new PipelineExecutionStillInProgressException("The pipeline execution is still in progress");
    }
    Stream<Output> outputs = outputPathsGetter.get().map(src -> getOutput(destDir, src));
    outputDownloader.accept(outputs);
  }

  private Output getOutput(Path destDir, Path src)
  {
    try {
      File dest = new File(destDir.concat(src.getLastSegment()).toString());
      return new Output(
          new FileOutputStream(dest, false),
          src
      );
    } catch (FileNotFoundException e) {
      throw new GivenDestDirDoesNotExistsException(e.getMessage());
    }
  }

}
