package pipeline.output;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.execution.Path;
import pipeline.ssh.SftpClient;

import java.util.stream.Stream;

/**
 * Downloads the outputs of the pipeline.
 */
public class OutputsDownloader {

  public static final Logger logger = LogManager.getLogger(OutputsDownloader.class);

  private final SftpClient sftpClient;
  private final Path baseDir;

  public OutputsDownloader(SftpClient sftpClient, Path baseDir)
  {
    this.sftpClient = sftpClient;
    this.baseDir = baseDir;
  }

  public void download(Stream<Output> outputs)
  {
    logger.info("Starting download of outputs");
    Stream<Output> mappedOutputs = outputs
        .map(o -> new Output(o.getDest(), baseDir.concat(o.getSrc())));
    sftpClient.start(channel -> mappedOutputs.forEach(channel::get));
    logger.info("Finished download of outputs");
  }
}
