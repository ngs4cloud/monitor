package pipeline.output;

import pipeline.execution.Path;

import java.io.OutputStream;

/**
 * Data structure which represents an output of a pipeline execution.
 */
public class Output {

  private final OutputStream dest;
  private final Path src;

  /**
   * @param dest - Location to where the pipeline output will be downloaded.
   * @param src - Path, regarding the cluster where the pipeline was executed,
   *             where the output is stored.
   */
  public Output(OutputStream dest, Path src)
  {
    this.dest = dest;
    this.src = src;
  }

  public OutputStream getDest()
  {
    return dest;
  }

  public Path getSrc()
  {
    return src;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Output output = (Output) o;

    if (!dest.equals(output.dest)) return false;
    if (!src.equals(output.src)) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result = dest.hashCode();
    result = 31 * result + src.hashCode();
    return result;
  }
}
