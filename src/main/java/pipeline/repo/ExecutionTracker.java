package pipeline.repo;

import pipeline.execution.Path;

import java.util.List;

/**
 * Data structure containing all the information regarding the
 * execution of a pipeline.
 */
public class ExecutionTracker {
  private final Long id;
  private final List<String> jobNames;
  private final List<Path> outputs;
  private final Path workDir;

  public ExecutionTracker(Long id, List<String> jobNames, List<Path> outputs, Path workDir)
  {
    this.id = id;
    this.jobNames = jobNames;
    this.outputs = outputs;
    this.workDir = workDir;
  }

  public long getId()
  {
    return id;
  }

  public List<String> getJobNames()
  {
    return jobNames;
  }

  public List<Path> getOutputs()
  {
    return outputs;
  }

  public Path getWorkDir()
  {
    return workDir;
  }
}
