package pipeline.repo;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.execution.Path;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import static java.util.stream.Collectors.*;
/**
 * Flat file repository which stores information regarding
 * the execution of pipelines.
 */
public class FlatFileExecutionTrackerRepo{

  public class NoSuchPipelineExecutionException extends RuntimeException{
    public NoSuchPipelineExecutionException(String message)
    {
      super(message);
    }
  }

  public static class CouldNotAccessFileException extends RuntimeException{
    public CouldNotAccessFileException(String message)
    {
      super(message);
    }
  }

  public static final Logger logger = LogManager.getLogger(FlatFileExecutionTrackerRepo.class);

  private final String dbFolder;
  private final ObjectMapper objectMapper;

  public FlatFileExecutionTrackerRepo(String dbFolder)
  {
    this.dbFolder = dbFolder;
    this.objectMapper = new ObjectMapper();
  }

  public ExecutionTracker get(long id)
  {
    try {
      logger.info("Getting execution tracker with id: " + id);
      ExecutionTracker tracker = internalGet(id);
      logger.info("Got execution tracker with id: " + id);
      return tracker;
    } catch (IOException e) {
      throw new CouldNotAccessFileException(e.getMessage());
    }
  }

  private ExecutionTracker internalGet(long id) throws IOException
  {
    File pipelineFile = new File(dbFolder + "/" + getExecutionInfoFileName(id));

    if(!pipelineFile.exists()){
      throw new NoSuchPipelineExecutionException("There is no execution with id: " + id);
    }

    ExecutionTrackerDto dto = new ObjectMapper().readValue(pipelineFile, ExecutionTrackerDto.class);
    return toExecutionTracker(dto);
  }

  private ExecutionTracker toExecutionTracker(ExecutionTrackerDto dto)
  {
    return new ExecutionTracker(dto.id,
        dto.chronosJobs,
        dto.outputs.stream().map(Path::make).collect(toList()),
        Path.make(dto.workDir));
  }

  protected String getExecutionInfoFileName(long id)
  {
    return "execution_" + id;
  }

  public long create(ExecutionTracker executionTracker)
  {
    try {
      logger.info("Creating execution tracker");
      long id = internalCreate(executionTracker);
      logger.info("Finished creating execution tracker");
      return id;
    } catch (IOException e) {
      throw new CouldNotAccessFileException(e.getMessage());
    }
  }

  private long internalCreate(ExecutionTracker executionTracker) throws IOException
  {
    File dbFolder = new File(this.dbFolder);
    long id = Arrays.stream(dbFolder.listFiles())
        .map(File::getName)
        .filter(name -> name.matches("^execution_\\d+$"))
        .mapToLong(name -> Long.parseLong(name.split("_")[1]))
        .max()
        .orElse(0L) + 1;
    objectMapper.writeValue(new File(this.dbFolder +"/"+ getExecutionInfoFileName(id)), toDto(executionTracker, id));
    return id;
  }

  private ExecutionTrackerDto toDto (ExecutionTracker executionTracker, long id)
  {
    return new ExecutionTrackerDto(id,
        executionTracker.getJobNames(),
        executionTracker.getOutputs().stream().map(Path::toString).collect(toList()),
        executionTracker.getWorkDir().toString());
  }
}
