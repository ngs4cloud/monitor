package pipeline.repo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * DTO of the information regarding the execution of a pipeline.
 */
public class ExecutionTrackerDto {

  public final long id;
  public final List<String> chronosJobs;
  public final List<String> outputs;
  public final String workDir;

  @JsonCreator
  public ExecutionTrackerDto(@JsonProperty("id") long id,
                             @JsonProperty("chronosJobs") List<String> chronosJobs,
                             @JsonProperty("outputs") List<String> outputs,
                             @JsonProperty("workDir") String workDir)
  {
    this.id = id;
    this.chronosJobs = chronosJobs;
    this.outputs = outputs;
    this.workDir = workDir;
  }
}
