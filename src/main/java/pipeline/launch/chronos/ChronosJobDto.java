package pipeline.launch.chronos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.List;

/**
 * DTO of the necessary information to launch a Chronos job.
 */
public class ChronosJobDto {

  public final String name;
  public final String owner;
  public final int mem;
  public final int disk;
  public final float cpus;
  public final String epsilon;
  public final String command;
  public final ContainerDto container;
  public final String schedule;
  public final List<String> parents;

  @JsonCreator
  public ChronosJobDto( @JsonProperty("name") String name,
                        @JsonProperty("owner") String owner,
                        @JsonProperty("mem") int mem,
                        @JsonProperty("disk") int disk,
                        @JsonProperty("cpus") float cpus,
                        @JsonProperty("epsilon") String epsilon,
                        @JsonProperty("command") String command,
                        @JsonProperty("container") ContainerDto container,
                        @JsonProperty("schedule") String schedule,
                        @JsonProperty("parents") List<String> parents)
  {
    this.name = name;
    this.owner = owner;
    this.mem = mem;
    this.disk = disk;
    this.cpus = cpus;
    this.schedule = schedule;
    this.epsilon = epsilon;
    this.command = command;
    this.container = container;
    this.parents = parents;
  }
}
