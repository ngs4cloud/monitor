package pipeline.launch.chronos;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.execution.Path;
import pipeline.execution.Task;
import pipeline.http.HttpClient;
import utils.TopologicalSorter;

import java.net.URI;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class ImpChronosJobsLauncher implements ChronosJobsLauncher{

  public static class JobLaunchFailedException extends RuntimeException{
    public final String failedJobName;

    public JobLaunchFailedException(String failedJobName)
    {
      this.failedJobName = failedJobName;
    }
  }

  public static final Logger logger = LogManager.getLogger(ImpChronosJobsLauncher.class);

  private final String chronosHost;
  private final int chronosPort;
  private final ChronosJobsFactory jobsFactory;
  private final HttpClient httpClient;

  public ImpChronosJobsLauncher(String chronosHost,
                                int chronosPort,
                                ChronosJobsFactory jobsFactory,
                                HttpClient httpClient)
  {
    this.chronosHost = chronosHost;
    this.chronosPort = chronosPort;
    this.jobsFactory = jobsFactory;
    this.httpClient = httpClient;
  }

  @Override
  public Stream<String> launch(Stream<Task> tasks, Stream<Path> directories, URI remoteInput)
  {
    List<ChronosJobDto> listJobs = jobsFactory.getJobs(tasks, directories, remoteInput).collect(toList());
    return internalLaunch(listJobs);
  }

  @Override
  public Stream<String> launch(Stream<Task> tasks, Stream<Path> directories, String inputName)
  {
    List<ChronosJobDto> listJobs = jobsFactory.getJobs(tasks, directories, inputName).collect(toList());
    return internalLaunch(listJobs);
  }

  private Stream<String> internalLaunch(List<ChronosJobDto> listJobs)
  {
    logger.info("Starting to make requests to launch chronos jobs");
    getTopologicallyOrdered(listJobs).forEach(this::launchJob);
    logger.info("Finished requests to launch chronos jobs");
    return listJobs.stream().map(job -> job.name);
  }

  /**
   * Topologically sorts the given Chronos jobs according to their parents.
   * This is necessary because if a job is launched first than it's parent Chronos
   * will never execute it.
   * @param listJobs - Chronos jobs to be topologically sorted.
   * @return Stream with the Chronos jobs topologically sorted.
   */
  private Stream<ChronosJobDto> getTopologicallyOrdered(List<ChronosJobDto> listJobs)
  {
    Function<ChronosJobDto, Stream<String>> extractParents = j ->
        j.parents == null ? Stream.empty() : j.parents.stream();
    return TopologicalSorter.sort(listJobs.stream(), j -> j.name, extractParents);
  }

  private void launchJob(ChronosJobDto job)
  {
    final String scheduleJobEndpoint = "http://" + chronosHost + ":" + chronosPort + "/scheduler/iso8601";
    final String dependentJobEndpoint = "http://" + chronosHost + ":" + chronosPort + "/scheduler/dependency";

    final String endpoint = job.schedule != null ? scheduleJobEndpoint : dependentJobEndpoint;
    HttpClient.HttpResponse<String> res = httpClient.postAsJson(endpoint, job);
    if (res.getStatus() != 204){
      throw new JobLaunchFailedException(job.name);
    }
  }
}
