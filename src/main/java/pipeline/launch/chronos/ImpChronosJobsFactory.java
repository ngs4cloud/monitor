package pipeline.launch.chronos;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.execution.Path;
import pipeline.execution.Task;

import java.net.URI;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class ImpChronosJobsFactory implements ChronosJobsFactory{

  public static final Logger logger = LogManager.getLogger(ImpChronosJobsFactory.class);

  public static final String EPSILON = "P1Y12M12D";
  private final String baseWorkDirectory;
  private final String baseJobName;
  private final String jobsOwner;
  private final String chronosHost;
  private final int chronosPort;
  private final String wgetDockerImage;
  private final String p7zipDockerImage;

  public ImpChronosJobsFactory(Path baseWorkDirectory, String baseJobName, String jobsOwner,
                               String chronosHost, int chronosPort,
                               String wgetDockerImage, String p7zipDockerImage)
  {
    this.baseWorkDirectory = baseWorkDirectory.toString();
    this.baseJobName = baseJobName;
    this.jobsOwner = jobsOwner;
    this.chronosHost = chronosHost;
    this.chronosPort = chronosPort;
    this.wgetDockerImage = wgetDockerImage;
    this.p7zipDockerImage = p7zipDockerImage;
  }

  @Override
  public Stream<ChronosJobDto> getJobs(Stream<Task> tasks, Stream<Path> directories, URI remoteInputURI)
  {
    logger.info("Starting to create chronos jobs to launch the pipeline");

    final String remoteInputName = "input";
    ChronosJobDto createBaseWorkDirAndDownloadInputJob = getCreateBaseWorkDirAndDownloadInputJob(remoteInputURI, remoteInputName);
    List<String> parents = Arrays.asList(createBaseWorkDirAndDownloadInputJob.name);
    ChronosJobDto decompressInputAndCreateWordDirsJob = getDecompressInputAndCreateWorkDirsJob(directories, "input", parents);

    List<ChronosJobDto> jobsFromTasks = getJobsFromTasks(tasks, decompressInputAndCreateWordDirsJob).collect(toList());
    ChronosJobDto launchCleanUpJobJob = getCleanUpJob(jobsFromTasks.stream());

    LinkedList<ChronosJobDto> jobs = new LinkedList<>(jobsFromTasks);
    jobs.addFirst(decompressInputAndCreateWordDirsJob);
    jobs.addFirst(createBaseWorkDirAndDownloadInputJob);
    jobs.addLast(launchCleanUpJobJob);

    logger.info("Finished creating chronos jobs to launch the pipeline");

    return jobs.stream();
  }

  @Override
  public Stream<ChronosJobDto> getJobs(Stream<Task> tasks, Stream<Path> directories, String inputName)
  {
    List<String> parents = null;
    ChronosJobDto decompressInputAndCreateWorkDirsJob = getDecompressInputAndCreateWorkDirsJob(directories, inputName, parents);
    List<ChronosJobDto> jobsFromTasks = getJobsFromTasks(tasks, decompressInputAndCreateWorkDirsJob).collect(toList());
    ChronosJobDto launchCleanUpJobJob = getCleanUpJob(jobsFromTasks.stream());

    LinkedList<ChronosJobDto> jobs = new LinkedList<>(jobsFromTasks);
    jobs.addFirst(decompressInputAndCreateWorkDirsJob);
    jobs.addLast(launchCleanUpJobJob);
    return jobs.stream();
  }

  /**
   * Returns a Chronos jobs that when executed will change the access permissions on the pipeline's outputs,
   * and will launch another Chronos jobs which will delete, within 7 days, the working directory of
   * the pipeline, therefore cleaning up all the files regarding the pipeline. Changing the access permission on the pipeline
   * outputs is necessary because outputs produced from docker jobs can only be accessed by the root user.
   * This means that without changing the access permission to allow non-root users to access the files,
   * only a root user would be able to download the outputs of the pipeline. The reason why this job doesn't
   * directly cleanup the files and instead launches another job to do it, is due to the fact
   * that Chronos does not support jobs that will only execute after a certain time after its parents
   * have successfully executed.
   * @param parents - Stream of Chronos jobs that the cleanup job will depend upon.
   * @return - Cleanup Chronos job.
   */
  private ChronosJobDto getCleanUpJob(Stream<ChronosJobDto> parents)
  {
    String command = "cd " + baseWorkDirectory + " && chmod -R 777 ./ && wget " + chronosHost + ":" +
        chronosPort + "/scheduler/iso8601 " +
        "--header \"Content-Type: application/json\"" +
        " --post-data \"{" +
        "\\\"name\\\": \\\"" + baseJobName + "_RmWorkDirs\\\"," +
        "\\\"epsilon\\\": \\\"" + EPSILON + "\\\"," +
        "\\\"schedule\\\": \\\"R1/$(date -d \"+7 days\" +'%Y-%m-%dT00:00:00Z')/"+ EPSILON +"\\\"," +
        "\\\"owner\\\": \\\"" + jobsOwner + "\\\"," +
        "\\\"retries\\\": 2," +
        "\\\"command\\\": \\\"rm -rf " + baseWorkDirectory + "\\\"" +
        "}\"";

    String schedule = null;
    return new ChronosJobDto(
        baseJobName + "_changeAccessPermissionLaunchRmWorkDirs",
        jobsOwner,
        100,
        100,
        1F,
        EPSILON,
        command,
        getContainerDto(wgetDockerImage),
        schedule,
        parents.map(j -> j.name).collect(toList())
    );
  }

  private Stream<ChronosJobDto> getJobsFromTasks(Stream<Task> tasks, ChronosJobDto setupJob)
  {
    return tasks.map(t ->
        t.getParents().isEmpty() ?
            getDependentJob(t, setupJob.name):
            getDependentJob(t));
  }

  /**
   * Returns a Chronos job that when executed will create the base work directory for
   * the execution of the pipeline, and will download to that directory the input of the
   * pipeline.
   * @param remoteInputURI - URI of the input of the pipeline.
   * @param remoteInputName - Name to give to the remote input.
   * @return DTO of the Chronos job.
   */
  private ChronosJobDto getCreateBaseWorkDirAndDownloadInputJob(URI remoteInputURI, String remoteInputName)
  {
    String command = "mkdir -p " + baseWorkDirectory + " && cd " + baseWorkDirectory +
        " && wget -O " + remoteInputName + " " + remoteInputURI.toString();
    String jobName = baseJobName + "wget";
    String scheduleOneRun = "R1//PT30M";
    List<String> parents = null;
    int mem = 100;
    int disk = 100;
    float cpus = 1F;

    return new ChronosJobDto(
        jobName,
        jobsOwner,
        mem,
        disk,
        cpus,
        EPSILON,
        command,
        getContainerDto(wgetDockerImage),
        scheduleOneRun,
        parents
    );
  }

  /**
   * Returns a Chronos job that when executed will decompress the input of
   * the pipeline and then will create all the necessary work directories for
   * the execution of the pipeline.
   * @param directories - Stream of the directories needed to execute the pipeline.
   * @param inputName - Name of the compress file that has all the inputs of the pipeline.
   * @param parents - List with the name of the Chronos jobs that need to have executed with success
   *                in order for this one to be executed.
   * @return DTO of a Chronos job.
   */
  private ChronosJobDto getDecompressInputAndCreateWorkDirsJob(Stream<Path> directories, String inputName, List<String> parents)
  {
    String command = "cd " + baseWorkDirectory + " && 7z x " + inputName;
    String mkdirCommand = directories.map( d -> "mkdir -p " + d.toString()).collect(joining(" && "));
    command = mkdirCommand.isEmpty() ? command : command + " && " + mkdirCommand;

    String jobName = baseJobName + "p7zip";
    String scheduleOneRun = "R1//PT30M";
    String schedule = parents == null ? scheduleOneRun : null;
    int mem = 1000;
    int disk = 500;
    float cpus = 1F;

    return new ChronosJobDto(
        jobName,
        jobsOwner,
        mem,
        disk,
        cpus,
        EPSILON,
        command,
        getContainerDto(p7zipDockerImage),
        schedule,
        parents
    );
  }

  private ChronosJobDto getDependentJob(Task task, String parent)
  {
    List<String> parents = Arrays.asList(parent);
    return getDependentJob(task, parents);
  }

  private ChronosJobDto getDependentJob(Task task)
  {
    List<String> parents = task.getParents().stream().map(id -> baseJobName + id).collect(toList());
    return getDependentJob(task, parents);
  }

  private ChronosJobDto getDependentJob(Task task, List<String> parents)
  {
    String schedule = null;
    return new ChronosJobDto(
        getJobName(task),
        jobsOwner,
        task.getMem(),
        task.getDisk(),
        task.getCpus(),
        EPSILON,
        getCommandExecutedOnBaseWorkDirectory(task),
        getContainerDto(task.getDockerImage()),
        schedule,
        parents
    );
  }

  private ContainerDto getContainerDto(String dockerImageName)
  {
    String volumeContainerPath = baseWorkDirectory;
    String volumeHostPath = baseWorkDirectory;
    return new ContainerDto(
        "DOCKER",
        dockerImageName,
        Arrays.asList(new ContainerDto.VolumeDto(volumeContainerPath, volumeHostPath, "RW"))
    );
  }

  private String getJobName(Task task)
  {
    return baseJobName + task.getId();
  }

  private String getCommandExecutedOnBaseWorkDirectory(Task task)
  {
    return "cd " + baseWorkDirectory + " && " + task.getCommand();
  }
}
