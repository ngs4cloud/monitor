package pipeline.launch.chronos;

import pipeline.execution.Path;
import pipeline.execution.Task;

import java.net.URI;
import java.util.stream.Stream;

/**
 * Converts a pipeline to the Chronos jobs needed for it to be executed.
 */
public interface ChronosJobsFactory {

  /**
   * Converts a pipeline to the Chronos jobs needed for it to be executed.
   * @param tasks - Stream of Tasks which constitute the pipeline.
   * @param directories - Stream of the directories needed to execute the pipeline.
   * @param remoteInput - URI of the input of the pipeline.
   * @return Stream of the Chronos jobs which constitute the pipeline.
   */
  Stream<ChronosJobDto> getJobs(Stream<Task> tasks, Stream<Path> directories, URI remoteInput);

  /**
   * Converts a pipeline to the Chronos jobs needed for it to be executed.
   * @param tasks - Stream of Tasks which constitute the pipeline.
   * @param directories - Stream of the directories needed to execute the pipeline.
   * @param inputName - Name of the input for the
   *                       execution of the pipeline which is accessible from the environment where
   *                       the Chronos jobs will be executed.
   * @return Stream of the Chronos jobs which constitute the pipeline.
   */
  Stream<ChronosJobDto> getJobs(Stream<Task> tasks, Stream<Path> directories, String inputName);
}
