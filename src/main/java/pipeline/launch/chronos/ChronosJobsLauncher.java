package pipeline.launch.chronos;

import pipeline.execution.Path;
import pipeline.execution.Task;

import java.net.URI;
import java.util.stream.Stream;

/**
 * Launches the execution of a pipeline as Chronos jobs.
 */
public interface ChronosJobsLauncher {

  /**
   * Launches the execution of a pipeline as Chronos jobs.
   * @param tasks - Stream of Tasks which constitute the pipeline.
   * @param directories - Stream of the directories needed to execute the pipeline.
   * @param remoteInput - URI of the input of the pipeline.
   * @return Stream with the name of all the Chronos jobs launched.
   */
  Stream<String> launch(Stream<Task> tasks, Stream<Path> directories, URI remoteInput);

  /**
   * Launches the execution of a pipeline as Chronos jobs.
   * @param tasks - Stream of Tasks which constitute the pipeline.
   * @param directories - Stream of the directories needed to execute the pipeline.
   * @param inputName - Name of the input for the
   *                       execution of the pipeline which is accessible from the environment where
   *                       the Chronos jobs will be executed.
   * @return Stream with the name of all the Chronos jobs launched.
   */
  Stream<String> launch(Stream<Task> tasks, Stream<Path> directories, String inputName);
}
