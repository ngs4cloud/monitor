package pipeline.launch.chronos;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * DTO of the information regarding the container of a Chronos job.
 */
public class ContainerDto {

  public final String type;
  public final String image;
  public final List<VolumeDto> volumes;

  @JsonCreator
  public ContainerDto(@JsonProperty("type") String type,
                      @JsonProperty("image") String image,
                      @JsonProperty("volumes") List<VolumeDto> volumes)
  {
    this.type = type;
    this.image = image;
    this.volumes = volumes;
  }

  public static class VolumeDto{
    public final String containerPath;
    public final String hostPath;
    public final String mode;

    @JsonCreator
    public VolumeDto(@JsonProperty("containerPath") String containerPath,
                     @JsonProperty("hostPath") String hostPath,
                     @JsonProperty("mode") String mode)
    {
      this.containerPath = containerPath;
      this.hostPath = hostPath;
      this.mode = mode;
    }
  }
}
