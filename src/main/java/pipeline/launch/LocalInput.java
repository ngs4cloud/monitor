package pipeline.launch;

import pipeline.execution.Path;

import java.io.File;

/**
 * Data structure which represents an input, for a pipeline,
 * which is stored in the machine executing this application.
 */
public class LocalInput {

  private final File src;
  private final Path dest;

  /**
   * @param src - Input for the pipeline.
   * @param dest - Path, regarding the cluster where the pipeline will be executed,
   *             to where the input should uploaded.
   */
  public LocalInput(File src, Path dest)
  {
    this.src = src;
    this.dest = dest;
  }

  public File getSrc()
  {
    return src;
  }

  public Path getDest()
  {
    return dest;
  }

  @Override
  public boolean equals(Object o)
  {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    LocalInput input = (LocalInput) o;

    if (!dest.equals(input.dest)) return false;
    if (!src.equals(input.src)) return false;

    return true;
  }

  @Override
  public int hashCode()
  {
    int result = src.hashCode();
    result = 31 * result + dest.hashCode();
    return result;
  }
}
