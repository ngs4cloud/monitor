package pipeline.launch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.execution.Path;
import pipeline.ssh.SftpClient;

import java.io.File;

/**
 * Initializes the cluster where the pipeline will be executed.
 */
public class ClusterInitializer {

  public static final Logger logger = LogManager.getLogger(ClusterInitializer.class);

  private final SftpClient sftpClient;

  public ClusterInitializer(SftpClient sftpClient)
  {
    this.sftpClient = sftpClient;
  }

  /**
   * Initializes the cluster where the pipeline will be executed.
   * The initialization consists in creating the work directory in the
   * environment where the pipeline will be executed and uploading the zip
   * file which contains the inputs for the pipeline execution.
   * @param input - input for the pipeline execution.
   * @param workDirectory - Work directory for the pipeline execution.
   */
  public void initCluster(File input, Path workDirectory)
  {
    logger.info("Starting to create base work directory and upload input file");

    String inputName = input.getName();

    sftpClient.start(channel -> {
      channel.mkdir(workDirectory);
      channel.put(new LocalInput(input, workDirectory.concat(Path.make(inputName))));
    });

    logger.info("Finished creating base work directory and upload input file");
  }
}
