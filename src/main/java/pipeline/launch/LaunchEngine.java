package pipeline.launch;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pipeline.execution.Execution;
import pipeline.execution.Path;
import pipeline.launch.chronos.ChronosJobsLauncher;
import pipeline.repo.ExecutionTracker;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.ToLongFunction;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 *
 */
public class LaunchEngine {

  public static final Logger logger = LogManager.getLogger(LaunchEngine.class);

  private final Function<InputStream, Execution> executionParser;
  private final BiConsumer<File, Path> clusterInitializer;
  private final ChronosJobsLauncher jobLauncher;
  private final ToLongFunction<ExecutionTracker> executionTrackerRepo;
  private final Path workDir;

  /**
   * @param executionParser - Parser an Execution from an InputStream which provides
   *                        an intermediate representation.
   * @param clusterInitializer - Initializes the cluster for the execution of a pipeline.
   *                           It is expected that it creates a folder corresponding to the given Path,
   *                           and that it uploads the given file from the machine
   *                           where this application is running to the cluster.
   * @param jobLauncher
   * @param executionTrackerRepo - Function that stores an ExecutionTracker and that returns an id
   *                             associated with the given ExecutionTracker.
   * @param workDir - Path of the work directory of the pipeline.
   */
  public LaunchEngine(Function<InputStream, Execution> executionParser,
                      BiConsumer<File, Path> clusterInitializer,
                      ChronosJobsLauncher jobLauncher,
                      ToLongFunction<ExecutionTracker> executionTrackerRepo,
                      Path workDir)
  {
    this.executionParser = executionParser;
    this.clusterInitializer = clusterInitializer;
    this.jobLauncher = jobLauncher;
    this.executionTrackerRepo = executionTrackerRepo;
    this.workDir = workDir;
  }

  public long run(InputStream irStream)
  {
    Execution execution = executionParser.apply(irStream);
    URI input = execution.getInput();
    Stream<String> jobNames = launchExecution(execution, input);
    ExecutionTracker executionTracker = new ExecutionTracker(null, jobNames.collect(toList()), execution.getOutputs(), workDir);
    long executionId = executionTrackerRepo.applyAsLong(executionTracker);
    return executionId;
  }

  private Stream<String> launchExecution(Execution execution, URI input)
  {
    if(input.getScheme().equals("file")){
      return launchExecutionProcedureForLocalInput(execution, input);
    }else{
      return launchExecutionProcedureForRemoteInput(execution, input);
    }
  }

  private Stream<String> launchExecutionProcedureForRemoteInput(Execution execution, URI input)
  {
    logger.info("Starting to launch execution for remote input");
    Stream<Path> directories = Stream.concat(
        Stream.of(workDir),
        execution.getDirectories().stream()
    );
    final Stream<String> launchedTasksNames = jobLauncher.launch(execution.getTasks().stream(), directories, input);
    logger.info("Finished launching execution for remote input");
    return launchedTasksNames;
  }

  private Stream<String> launchExecutionProcedureForLocalInput(Execution execution, URI input)
  {
    logger.info("Starting to launch execution for local input");
    File fileInput = new File(input);
    clusterInitializer.accept(fileInput, workDir);
    final Stream<String> launchedTasksNames = jobLauncher.launch(execution.getTasks().stream(),
        execution.getDirectories().stream(), fileInput.getName());
    logger.info("Finished launching execution for local input");
    return launchedTasksNames;
  }
}
