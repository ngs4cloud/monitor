package pipeline.http;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;
import java.util.Map;

public interface HttpClient {

  public static class RequestFailedException extends RuntimeException{
    public RequestFailedException(String message)
    {
      super(message);
    }
  }

  /**
   * Sends an http POST request with it's body in json format.
   * @param url - String containing the url to where the POST will be made.
   * @param body - Object which will be converted to json and sent as the request body.
   * @return Response to the request.
   */
  HttpResponse<String> postAsJson(String url, Object body);

  /**
   * Sends an http GET request with the accept header as application/json.
   * @param url - String containing the url to where the GET will be made.
   * @param type - Type of the body of the returned HttpResponse. This was
   *             necessary because there situations where the body of the HttpResponse
   *             is a generic class, which means that due to type erasure the generic
   *             type of the generic class would have been lost at run time.
   * @return Response to the request.
   */
  <T> HttpResponse<T> getAsJson(String url, TypeReference<T> type);

  public interface HttpResponse<T>{

    int getStatus();

    String getStatusText();

    Map<String, List<String>> getHeaders();

    T getBody();
  }
}
