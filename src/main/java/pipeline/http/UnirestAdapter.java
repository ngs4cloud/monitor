package pipeline.http;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * Implementation of an Http Client using as base Unirest library.
 */
public class UnirestAdapter implements HttpClient {

  public static class JsonConversionException extends RuntimeException{
    public JsonConversionException(String message)
    {
      super(message);
    }
  }

  public static final Logger logger = LogManager.getLogger(UnirestAdapter.class);

  @Override
  public HttpResponse<String> postAsJson(String url, Object body)
  {
    try {
      return new UnirestHttpResponseAdapter<>(
         Unirest.post(url).header("Content-Type", "application/json").body(toJson(body)).asString(),
         Function.<String>identity());
    } catch (UnirestException e) {
      throw new HttpClient.RequestFailedException(e.getMessage());
    }
  }

  private static String toJson(Object obj)
  {
    try {
      ObjectMapper mapper = new ObjectMapper();
      mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
      return mapper.writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      throw new JsonConversionException(e.getMessage());
    }
  }

  @Override
  public <T> HttpResponse<T> getAsJson(String url, TypeReference<T> type)
  {
    try {
      return new UnirestHttpResponseAdapter<>(
          Unirest.get(url).asJson(),
          jsonNode -> toObject(jsonNode, type));
    } catch (UnirestException e) {
      throw new HttpClient.RequestFailedException(e.getMessage());
    }
  }

  private static <T> T toObject(JsonNode jsonNode, TypeReference<T> type)
  {
    try {
      logger.info("Starting to map json to in memory object");
      final ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      final T obj = mapper.readValue(jsonNode.toString(), type);
      logger.info("Finished mapping json to in memory object");
      
      return obj;
    } catch (IOException e) {
      throw new JsonConversionException(e.getMessage());
    }
  }

  public static class UnirestHttpResponseAdapter<E, T> implements HttpClient.HttpResponse<T>
  {
    private final com.mashape.unirest.http.HttpResponse<E> response;
    private final Function<E, T> mapper;

    public UnirestHttpResponseAdapter(com.mashape.unirest.http.HttpResponse<E> response,
                                      Function<E, T> mapper)
    {
      this.response = response;
      this.mapper = mapper;
    }

    @Override
    public int getStatus()
    {
      return response.getStatus();
    }

    @Override
    public String getStatusText()
    {
      return response.getStatusText();
    }

    @Override
    public Map<String, List<String>> getHeaders()
    {
      return response.getHeaders();
    }

    @Override
    public T getBody()
    {
      return mapper.apply(response.getBody());
    }
  }
}
