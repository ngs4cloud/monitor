package pipeline.ssh;

import pipeline.execution.Path;
import pipeline.launch.LocalInput;
import pipeline.output.Output;

import java.util.function.Consumer;

public interface SftpClient {

  public static class AuthenticationFailedException extends RuntimeException {
    public AuthenticationFailedException(String message)
    {
      super(message);
    }
  }

  /**
   * Starts an Sftp connection and performs the actions specified upon it.
   * @param actions - actions to perform in the Sftp connection.
   */
  void start(Consumer<SftpChannel> actions);

  interface SftpChannel {

    public static class SftpInteractionException extends RuntimeException{
      public SftpInteractionException(String message)
      {
        super(message);
      }
    }

    public static class FileGivenAsInputDoesNotExistException extends RuntimeException{
      public FileGivenAsInputDoesNotExistException(String message)
      {
        super(message);
      }
    }

    /**
     * Uploads given input.
     * @param input - local input to be uploaded.
     */
    void put(LocalInput input);

    /**
     * Downloads specified output.
     * @param output - Output to be download
     */
    void get(Output output);

    /**
     * Removes a specified file.
     * @param path - Path of the file to be removed.
     */
    void rm(Path path);

    /**
     * Makes a directory
     * @param path - Directory to be made.
     */
    void mkdir(Path path);
  }
}
