package pipeline.ssh;

import com.jcraft.jsch.*;
import pipeline.execution.Path;
import pipeline.launch.LocalInput;
import pipeline.output.Output;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;
import java.util.function.Consumer;

/**
 * Implementation of an Sftp Client using as base JSch library.
 */
public class JSchSftpClientAdapter implements SftpClient {

  private final String user;
  private final String host;
  private final int port;
  private final SftpProgressMonitor monitor;

  private String password;

  private String keyPairName;
  private byte[] publicKey;
  private byte[] privateKey;
  private byte[] privateKeyPassword;

  public static SftpClient makePasswordAuthenticationSftpClient(String user,
                                                                String host,
                                                                int port,
                                                                String password,
                                                                SftpProgressMonitor monitor){
    return new JSchSftpClientAdapter(user, host, port, password, monitor);
  }

  public static SftpClient makePublicKeyAuthenticationSftpClient(String user,
                                                                String host,
                                                                int port,
                                                                String keyPairName,
                                                                byte[] publicKey,
                                                                byte[] privateKey,
                                                                byte[] privateKeyPassword,
                                                                SftpProgressMonitor monitor){
    return new JSchSftpClientAdapter(user, host, port, keyPairName,
        publicKey, privateKey ,privateKeyPassword, monitor);
  }

  private JSchSftpClientAdapter(String user, String host, int port, SftpProgressMonitor monitor)
  {
    this.user = user;
    this.host = host;
    this.port = port;
    this.monitor = monitor;
  }

  private JSchSftpClientAdapter(String user, String host, int port, String password, SftpProgressMonitor monitor)
  {
    this(user, host, port, monitor);
    this.password = password;
  }

  private JSchSftpClientAdapter(String user, String host, int port, String keyPairName,
                                byte[] publicKey, byte[] privateKey, byte[] privateKeyPassword, SftpProgressMonitor monitor)
  {
    this(user, host, port, monitor);
    this.keyPairName = keyPairName;
    this.publicKey = publicKey;
    this.privateKey = privateKey;
    this.privateKeyPassword = privateKeyPassword;
  }

  @Override
  public void start(Consumer<SftpChannel> actions)
  {
    try {
      internalStart(actions);
    } catch (JSchException e) {
      throw new AuthenticationFailedException(e.getMessage());
    }
  }

  private void internalStart(Consumer<SftpChannel> actions) throws JSchException
  {
    Session session = getSession();
    ChannelSftp channelSftp = null;
    try {
      Properties config = new Properties();
      config.put("StrictHostKeyChecking", "no");
      session.setConfig(config);
      session.connect();

      channelSftp = (ChannelSftp) session.openChannel("sftp");
      channelSftp.connect();
      actions.accept(new JschChannelSftpAdapter(channelSftp, monitor));
    }finally {
      if(channelSftp != null) {
        channelSftp.disconnect();
      }
      session.disconnect();
    }
  }

  private Session getSession() throws JSchException
  {
    JSch jsch = new JSch();
    if (keyPairName != null)
      jsch.addIdentity(keyPairName, publicKey, privateKey, privateKeyPassword);

    Session session = jsch.getSession(user, host, port);
    if (password != null)
      session.setPassword(password);

    return session;
  }

  private static class JschChannelSftpAdapter implements SftpClient.SftpChannel {

    private final ChannelSftp channelSftp;
    private final SftpProgressMonitor monitor;

    public JschChannelSftpAdapter(ChannelSftp channelSftp, SftpProgressMonitor monitor)
    {
      this.channelSftp = channelSftp;
      this.monitor = monitor;
    }

    @Override
    public void put(LocalInput input)
    {
      try {
        InputStream is = new FileInputStream(input.getSrc());
        channelSftp.put(is, input.getDest().toString(), monitor);
      } catch (SftpException e) {
        throw new SftpInteractionException(e.getMessage());
      } catch (FileNotFoundException e) {
        throw new FileGivenAsInputDoesNotExistException(e.getMessage());
      }
    }

    @Override
    public void get(Output output)
    {
      try {
        channelSftp.get(output.getSrc().toString(), output.getDest(), monitor);
      } catch (SftpException e) {
        throw new SftpInteractionException(e.getMessage());
      }
    }

    @Override
    public void rm(Path path)
    {
      try {
        channelSftp.rm(path.toString());
      } catch (SftpException e) {
        throw new SftpInteractionException(e.getMessage());
      }
    }

    @Override
    public void mkdir(Path path)
    {
      try {
        channelSftp.mkdir(path.toString());
      } catch (SftpException e) {
        throw new SftpInteractionException(e.getMessage());
      }
    }
  }
}
