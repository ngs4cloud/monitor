package pipeline.ssh;

import com.jcraft.jsch.SftpProgressMonitor;
import pipeline.execution.Path;

import java.io.OutputStream;
import java.io.OutputStreamWriter;

import static utils.IOUtils.writeln;

public class ProgressMonitor implements SftpProgressMonitor {

  private final OutputStreamWriter outWriter;
  private float max;
  private float sumTransferredBytes;

  public ProgressMonitor(OutputStream out)
  {
    this.outWriter = new OutputStreamWriter(out);
  }

  @Override
  public void init(int op, String srcFile, String destFile, long max)
  {
    this.max = max;
    this.sumTransferredBytes = 0F;
    if(SftpProgressMonitor.GET == op){
      String fileName = Path.make(srcFile).getLastSegment().toString();
      writeln("Starting download of " + fileName, outWriter);
    }else{
      String fileName = Path.make(destFile).getLastSegment().toString();
      writeln("Starting upload of " + fileName, outWriter);
    }
  }

  @Override
  public boolean count(long transferredBytes)
  {
    this.sumTransferredBytes += transferredBytes;

    if(this.max != SftpProgressMonitor.UNKNOWN_SIZE) {
      int completePercentage = (int) ((sumTransferredBytes/max) * 100);
      writeln(completePercentage + "%", outWriter);
    }else{
      writeln("Transferred: " + sumTransferredBytes + " Bytes", outWriter);
    }
    return true;
  }

  @Override
  public void end(){
    System.out.println("Finished");
  }
}
