package testutils;

import junit.framework.AssertionFailedError;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.function.ToIntFunction;

/**
 *
 */
public class AssertUtils {

  public static <T> void AssertIsInAscendingOrdered(Iterable<T> iterable, ToIntFunction<T> valGetter)
  {
    Iterator<T> iterator = iterable.iterator();
    if (!iterator.hasNext()) {
      return;
    }

    int prev = valGetter.applyAsInt(iterator.next());
    while (iterator.hasNext()) {
      int cur = valGetter.applyAsInt(iterator.next());
      if (prev > cur) {
        throw new AssertionFailedError("Iterable is not ordered");
      }
      prev = cur;
    }
  }

  public static <T> void AssertCollectionsHaveTheSameElements(Collection<T> expected, Collection<T> actual)
  {
    if(expected.size() != actual.size()){
      throw new AssertionFailedError("Collections have different sizes");
    }
    if(!new HashSet<>(expected).equals(new HashSet<>(actual))){
      throw new AssertionFailedError("Collections have different elements");
    }
  }
}
