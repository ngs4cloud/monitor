package main;

import org.junit.Test;

import java.io.InputStream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
/**
 *
 */
public class TestConfigsGetter {

  @Test(expected = ConfigsGetter.InvalidConfigsException.class)
  public void givenInputWronglyFormattedThrowsInvalidConfigsException() throws Exception
  {
    ConfigsGetter.make(getClass().getClassLoader().getResourceAsStream("malformed_configs.txt"));
  }

  @Test(expected = ConfigsGetter.InvalidConfigsException.class)
  public void givenAnUnknownConfigurationThrowsInvalidConfigsException() throws Exception
  {
    ConfigsGetter.make(getClass().getClassLoader().getResourceAsStream("unknown_configs.txt"));
  }

  @Test
  public void whenProvidedWithAFileThatDoesNotHaveAllRequiredConfigsThrowsNotAllRequiredConfigsHaveBeenDefined() throws Exception
  {
    try {
      ConfigsGetter.make(getClass().getClassLoader().getResourceAsStream("incomplete_configs.txt"));
    }catch (ConfigsGetter.NotAllRequiredConfigsAreDefinedException e){
      assertThat(e.undefinedConfigs.contains(ConfigsGetter.ConfigKey.SSH_HOST), is(true));
      assertThat(e.undefinedConfigs.contains(ConfigsGetter.ConfigKey.SSH_PORT), is(true));
      assertThat(e.undefinedConfigs.size(), is(2));
    }
  }

  @Test
  public void ignoresEmptyLines() throws Exception
  {
    ConfigsGetter.make(getClass().getClassLoader().getResourceAsStream("config_with_empty_lines.txt"));
  }

  @Test
  public void canGetAllConfigs() throws Exception
  {
    InputStream is = getClass().getClassLoader().getResourceAsStream("configs.txt");
    ConfigsGetter configsGetter = ConfigsGetter.make(is);

    assertThat(configsGetter.getSshHost(), is("127.0.0.1"));
    assertThat(configsGetter.getSshPort(), is(22));
    assertThat(configsGetter.getSshUser(), is("ngs4cloud"));
    assertThat(configsGetter.getSshPass().get(), is("pass123"));
    assertThat(configsGetter.getSshPrivateKeyPath().get(), is("C:\\Users\\NGS4Cloud\\privkey.ppk"));
    assertThat(configsGetter.getSshPrivateKeyPass().get(), is("privKeyPass123"));
    assertThat(configsGetter.getSshPublicKeyPath().get(), is("C:\\Users\\NGS4Cloud\\pubkey"));
    assertThat(configsGetter.getChronosHost(), is("127.0.0.1"));
    assertThat(configsGetter.getChronosPort(), is(8080));
    assertThat(configsGetter.getClusterSharedDirPath(), is("/shared"));
    assertThat(configsGetter.getExecutionTrackerRepoPath(), is("C:\\Users\\NGS4Cloud\\Repo"));
    assertThat(configsGetter.getPipelineOwner(), is("user@example.com"));
    assertThat(configsGetter.getWgetDockerImage(), is("jnforja/wget"));
    assertThat(configsGetter.getP7zipDockerImage(), is("jnforja/7zip"));
  }
}
