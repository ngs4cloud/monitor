package utils;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Stream;

import static junit.framework.TestCase.assertEquals;

/**
 *
 */
public class TestTopologicalSorter {

  @Test(expected = TopologicalSorter.KeysMustBeUniqueException.class)
  public void whenKeysAreNotUniqueThrowsKeysMustBeUniqueException() throws Exception
  {
    List<SonParent> sonParents = Arrays.asList(
        new SonParent("1", Collections.emptyList()),
        new SonParent("2", Arrays.asList("1")),
        new SonParent("2", Arrays.asList("1"))
    );
    TopologicalSorter.sort(sonParents.stream(), SonParent::getSon, SonParent::getParents);
  }

  @Test(expected = TopologicalSorter.ThereAreCyclicalDependenciesException.class)
  public void whenThereAreCyclicalDependenciesThrowsThereAreCyclicalDependenciesException() throws Exception
  {
    List<SonParent> sonParents = Arrays.asList(
        new SonParent("1", Arrays.asList("2")),
        new SonParent("2", Arrays.asList("1"))
    );
    TopologicalSorter.sort(sonParents.stream(), SonParent::getSon, SonParent::getParents);

  }

  @Test
  public void canOrderTopologically() throws Exception
  {
    List<SonParent> sonParents = Arrays.asList(
        new SonParent("1", Collections.emptyList()),
        new SonParent("2", Arrays.asList("1")),
        new SonParent("3", Arrays.asList("2")),
        new SonParent("4", Arrays.asList("3")),
        new SonParent("5", Collections.emptyList()),
        new SonParent("6", Arrays.asList("4","5"))
    );
    Stream<SonParent> ordered = TopologicalSorter.sort(sonParents.stream(), SonParent::getSon, SonParent::getParents);

    HashSet<String> alreadySeenSons = new HashSet<>();
    ordered.forEach(s -> {
      long nNotSeenParents = s.getParents().filter(p -> !alreadySeenSons.contains(p)).count();
      assertEquals(0, nNotSeenParents);
      alreadySeenSons.add(s.getSon());
    });
  }

  private static class SonParent{

    private final String son;
    private final List<String> parents;

    private SonParent(String son, List<String> parents)
    {
      this.son = son;
      this.parents = parents;
    }

    public String getSon()
    {
      return son;
    }

    public Stream<String> getParents()
    {
      return parents.stream();
    }
  }
}
