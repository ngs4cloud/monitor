package pipeline.ssh;

import com.jcraft.jsch.SftpProgressMonitor;
import org.apache.commons.io.IOUtils;
import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.subsystem.sftp.SftpSubsystemFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import pipeline.execution.Path;
import pipeline.launch.LocalInput;
import pipeline.output.Output;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class TestJSchSftpClientAdapter {

  @Rule
  public TemporaryFolder tempFolder = new TemporaryFolder();

  public static final String HOST = "localhost";
  public static final int PORT = 9000;
  public static final String USER = "jnforja";
  public static final String PASS = "pass123";

  private static final Consumer<SftpClient.SftpChannel> NOOP = sftpChannel -> {};

  private static SshServer sshd;

  @BeforeClass
  public static void startSshServer() throws Exception
  {
    sshd = SshServer.setUpDefaultServer();

    sshd.setPort(PORT);
    sshd.setPasswordAuthenticator((user, pass, serverSession) -> USER.equals(user) && PASS.equals(pass));
    sshd.setPublickeyAuthenticator((user, publicKey, serverSession) -> USER.equals(user));
    sshd.setKeyPairProvider(new SimpleGeneratorHostKeyProvider());

    List<NamedFactory<Command>> namedFactoryList = new ArrayList<>();
    namedFactoryList.add(new SftpSubsystemFactory());
    sshd.setSubsystemFactories(namedFactoryList);

    sshd.start();
  }

  @AfterClass
  public static void stopSshServer() throws Exception
  {
    sshd.stop();
  }
  @Test(expected = JSchSftpClientAdapter.AuthenticationFailedException.class)
  public void throwsExceptionWhenAuthenticationThroughPasswordFails() throws Exception
  {
    SftpClient sftpClient = JSchSftpClientAdapter.makePasswordAuthenticationSftpClient(USER, HOST, PORT,
        "invalidPass", new MockSftpProgressMonitor());
    sftpClient.start(NOOP);
  }

  @Test
  public void canAuthenticateWithPassword() throws Exception
  {
    SftpClient sftpClient = JSchSftpClientAdapter.makePasswordAuthenticationSftpClient(USER, HOST, PORT,
        PASS, new MockSftpProgressMonitor() );
    sftpClient.start(NOOP);
  }

  @Test(expected = JSchSftpClientAdapter.AuthenticationFailedException.class)
  public void throwsExceptionWhenAuthenticationThroughPublicKeyFails() throws Exception
  {
    String keyPairName = "name";
    byte[] pubKey = IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("pipeline/ssh/test_pub_key"));
    byte[] privKey = IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("pipeline/ssh/test_priv_key.ppk"));
    byte[] invalidPrivKeyPass = new byte[0];

    SftpClient sftpClient = JSchSftpClientAdapter
        .makePublicKeyAuthenticationSftpClient(USER, HOST, PORT, keyPairName, pubKey, privKey,
            invalidPrivKeyPass, new MockSftpProgressMonitor() );
    sftpClient.start(NOOP);
  }

  @Test
  public void canAuthenticateWithPrivateKey() throws Exception
  {
    String keyPairName = "name";
    byte[] pubKey = IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("pipeline/ssh/test_pub_key"));
    byte[] privKey = IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("pipeline/ssh/test_priv_key.ppk"));
    byte[] privKeyPass = IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("pipeline/ssh/test_priv_key_pass.txt"));

    SftpClient sftpClient = JSchSftpClientAdapter
        .makePublicKeyAuthenticationSftpClient(USER, HOST, PORT, keyPairName, pubKey, privKey,
            privKeyPass, new MockSftpProgressMonitor());
    sftpClient.start(NOOP);
  }

  @Test(expected = SftpClient.SftpChannel.FileGivenAsInputDoesNotExistException.class)
  public void givenLocalInputWithAnInexistentFileThrowsFileGivenAsInputDoesNotExistException() throws Exception
  {
    File file = new File("doesNotExist");
    LocalInput localInput = new LocalInput(file, Path.make("/pipes/test"));
    SftpClient sftpClient = JSchSftpClientAdapter.makePasswordAuthenticationSftpClient(USER, HOST, PORT,
        PASS, new MockSftpProgressMonitor());
    sftpClient.start(channel -> {
      channel.put(localInput);
    });
  }

  @Test
  public void canUploadAndThenDownloadUploadedFile() throws Exception
  {
    String keyPairName = "name";
    byte[] pubKey = IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("pipeline/ssh/test_pub_key"));
    byte[] privKey = IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("pipeline/ssh/test_priv_key.ppk"));
    byte[] privKeyPass = IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("pipeline/ssh/test_priv_key_pass.txt"));

    SftpClient sftpClient = JSchSftpClientAdapter
        .makePublicKeyAuthenticationSftpClient(USER, HOST, PORT, keyPairName, pubKey, privKey,
            privKeyPass, new MockSftpProgressMonitor());
    sftpClient.start(channel -> {
      try {
        final String content = "Test file content";
        File input = tempFolder.newFile("testInput");
        Files.write(Paths.get(input.toURI()), content.getBytes(), StandardOpenOption.CREATE);
        Path remoteDestPath = Path.make("testFile");
        channel.put(new LocalInput(input, remoteDestPath));

        Path localDestPath = remoteDestPath;
        OutputStream outputStream = new ByteArrayOutputStream();
        Output output = new Output(outputStream, localDestPath);
        channel.get(output);

        assertThat(content, is(outputStream.toString()));
        channel.rm(remoteDestPath);
      }catch (IOException e){
        throw new RuntimeException(e);
      }
    });
  }

  private static class MockSftpProgressMonitor implements SftpProgressMonitor{

    @Override
    public void init(int i, String s, String s1, long l)
    {

    }

    @Override
    public boolean count(long l)
    {
      return false;
    }

    @Override
    public void end()
    {

    }
  }
}
