package pipeline.status.chronos;

import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Test;
import pipeline.http.HttpClient;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class TestChronosJobsStatusGetter {

  public static final String HOST = "test.com";
  public static final int PORT = 8080;

  @Test(expected = ChronosJobsStatusGetter.CouldNotGetChronosJobStatusException.class)
  public void whenFailedToGetStatusOfChronosJobsThrowsCouldNotGetChronosJobStatusException() throws Exception
  {
    new ChronosJobsStatusGetter(HOST, PORT, new MockFailureHttpClient()).getJobsStatus();
  }

  @Test
  public void canGetStatusOfChronosJobs() throws Exception
  {
    List<ChronosJobStatusDto> jobStatusDtos = Arrays.asList(
        new ChronosJobStatusDto("job1", 0, 0, 2),
        new ChronosJobStatusDto("job2", 0, 0, 2));
    MockSuccessHttpClient mockClient = new MockSuccessHttpClient(jobStatusDtos);
    ChronosJobsStatusGetter statusGetter =
        new ChronosJobsStatusGetter(HOST, PORT, mockClient);

    Stream<ChronosJobStatus> status = statusGetter.getJobsStatus();

    List<ChronosJobStatus> expectedJobsStatus = Arrays.asList(
        new ChronosJobStatus("job1", 0, 0, 2),
        new ChronosJobStatus("job2", 0, 0, 2));
    assertThat(status.collect(Collectors.toList()), is(expectedJobsStatus));
    assertThat(mockClient.urlGivenToGetAsJson, is("http://" + HOST + ":" + PORT + "/scheduler/jobs"));
    assertThat(mockClient.typeGivenToGetAsJson.getType().getTypeName(),
        is(new TypeReference<List<ChronosJobStatusDto>>(){}.getType().getTypeName()));
  }

  private static class MockSuccessHttpClient implements HttpClient{

    public final List<ChronosJobStatusDto> jobsStatusToReturn;
    public String urlGivenToGetAsJson;
    public TypeReference typeGivenToGetAsJson;

    private MockSuccessHttpClient(List<ChronosJobStatusDto> jobsStatusToReturn)
    {
      this.jobsStatusToReturn = jobsStatusToReturn;
    }

    @Override
    public HttpResponse<String> postAsJson(String url, Object body)
    {
      return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> HttpResponse<T> getAsJson(String url, TypeReference<T> type)
    {
      this.urlGivenToGetAsJson = url;
      this.typeGivenToGetAsJson = type;
      return (HttpResponse<T>)new MockHttpResponse<>(200, jobsStatusToReturn);
    }
  }

  private static class MockFailureHttpClient implements HttpClient{

    @Override
    public HttpResponse<String> postAsJson(String url, Object body)
    {
      return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> HttpResponse<T> getAsJson(String url, TypeReference<T> type)
    {
      return (HttpResponse<T>)new MockHttpResponse<>(500, null);
    }
  }

  private static class MockHttpResponse<T> implements HttpClient.HttpResponse<T> {

    private final int status;
    private final T body;

    public MockHttpResponse(int status, T body)
    {
      this.status = status;
      this.body = body;
    }

    @Override
    public int getStatus()
    {
      return status;
    }

    @Override
    public String getStatusText()
    {
      return null;
    }

    @Override
    public Map<String, List<String>> getHeaders()
    {
      return null;
    }

    @Override
    public T getBody()
    {
      return body;
    }
  }
}
