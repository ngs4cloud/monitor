package pipeline.status;

import org.junit.Before;
import org.junit.Test;
import pipeline.execution.Path;
import pipeline.repo.ExecutionTracker;
import pipeline.status.chronos.ChronosJobStatus;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class TestExecutionStatusGetter {

  private MockExecutionTrackerGetter mockTrackerGetter;
  private MockChronosJobsStatusGetter mockStatusGetter;

  @Before
  public void setUp() throws Exception
  {
    mockTrackerGetter = new MockExecutionTrackerGetter();
    mockStatusGetter = new MockChronosJobsStatusGetter();
  }

  @Test(expected = ExecutionStatusGetter.NotAllPipelineJobsAreStagedException.class)
  public void whenNotAllJobsAssociatedWithTheExecutionAreReturnedThrowsNotAllPipelineJobsAreStagedException() throws Exception
  {
    ExecutionStatusGetter statusGetter = new ExecutionStatusGetter(mockTrackerGetter::getNamesOfFinishedAndFailedJobs,
        mockStatusGetter::getEmpty);

    statusGetter.get();
  }

  @Test
  public void whenAllJobsHaveFinishedReturnsFinishedExecutionStatus() throws Exception
  {
    ExecutionStatusGetter engine = new ExecutionStatusGetter(mockTrackerGetter::getNamesOfFinishedJobs, mockStatusGetter::get);

    ExecutionStatus executionStatus = engine.get();

    assertThat(executionStatus.getMessage(), is("The pipeline execution has finished with success."));
    assertThat(executionStatus.hasFinished(), is(true));
  }

  @Test
  public void whenOneJobHasFailedReturnsAFailedExecutionStatus() throws Exception
  {
    ExecutionStatusGetter statusGetter = new ExecutionStatusGetter(mockTrackerGetter::getNamesOfFinishedAndFailedJobs,
        mockStatusGetter::get);

    ExecutionStatus executionStatus = statusGetter.get();

    assertThat(executionStatus.getMessage(), is("The pipeline execution has failed."));
    assertThat(executionStatus.hasFailed(), is(true));
  }

  @Test
  public void whenOneJobIsStillInProgressReturnsAnInProgressExecutionStatus() throws Exception
  {
    ExecutionStatusGetter statusGetter = new ExecutionStatusGetter(mockTrackerGetter::getNamesOfFinishedAndInProgressJobs,
        mockStatusGetter::get);

    ExecutionStatus executionStatus = statusGetter.get();

    assertThat(executionStatus.getMessage(), is("In progress, 2/3 jobs have finished."));
    assertThat(executionStatus.isInProgress(), is(true));
  }

  public static class MockExecutionTrackerGetter{

    public ExecutionTracker getNamesOfFinishedJobs()
    {
      return new ExecutionTracker(1L,
          Arrays.asList("finishedJob1", "finishedJob2"),
          Collections.emptyList(), Path.make("/work"));
    }

    public ExecutionTracker getNamesOfFinishedAndFailedJobs()
    {
      return new ExecutionTracker(1L,
          Arrays.asList("finishedJob1", "finishedJob2", "failedJob"),
          Collections.emptyList(), Path.make("/work") );
    }

    public ExecutionTracker getNamesOfFinishedAndInProgressJobs()
    {
      return new ExecutionTracker(1L,
          Arrays.asList("finishedJob1", "finishedJob2", "inProgressJob"),
          Collections.emptyList(), Path.make("/work"));
    }
  }

  public static class MockChronosJobsStatusGetter{
    public Stream<ChronosJobStatus> get()
    {
      return Arrays.asList(
              new ChronosJobStatus("finishedJob1", 1, 0, 2),
              new ChronosJobStatus("finishedJob2", 1, 0, 2),
              new ChronosJobStatus("failedJob", 0, 2, 2),
              new ChronosJobStatus("inProgressJob", 0, 0, 2)
              ).stream();
    }

    public Stream<ChronosJobStatus> getEmpty()
    {
      return Stream.empty();
    }
  }
}
