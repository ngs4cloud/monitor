package pipeline.repo;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import pipeline.execution.Path;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class TestFlatFileExecutionTrackerRepo {

  @Rule
  public TemporaryFolder tempFolder = new TemporaryFolder();

  public static final String DB_FOLDER_NAME = "flatTestDb";

  private FlatFileExecutionTrackerRepo repo;
  private File dbFolder;

  @Before
  public void setUp() throws Exception
  {
    dbFolder = tempFolder.newFolder(DB_FOLDER_NAME);
    repo = new FlatFileExecutionTrackerRepo(dbFolder.getPath());
  }

  @Test(expected = FlatFileExecutionTrackerRepo.NoSuchPipelineExecutionException.class)
  public void givenTheIdThatDoNotCorrespondToAPipelineThrowsNoSuchPipelineException() throws Exception
  {
    repo.get(42L);
  }

  @Test
  public void givenIdOfAPipelineReturnsIt() throws Exception
  {
    final Long id = 1L;
    File pipelineFile = tempFolder.newFile(DB_FOLDER_NAME + "/" + repo.getExecutionInfoFileName(id));
    String jsonPipeline = "{\"id\":" + id + "," +
        "\"chronosJobs\":[\"first\",\"second\"]," +
        "\"outputs\":[\"/pipes/out1\",\"/pipes/out2\"]," +
        "\"workDir\":\"work\"}";
    Files.write(Paths.get(pipelineFile.toURI()), jsonPipeline.getBytes(), StandardOpenOption.CREATE);

    ExecutionTracker executionTracker = repo.get(id);

    assertThat(executionTracker.getId(), is(id));
    assertThat(executionTracker.getJobNames(), is(Arrays.asList("first", "second")));
    assertThat(executionTracker.getOutputs(), is(Arrays.asList(Path.make("/pipes/out1"), Path.make("/pipes/out2"))));
    assertThat(executionTracker.getWorkDir(), is(Path.make("work")));
  }

  @Test
  public void canStorePipeline() throws Exception
  {
    ExecutionTracker executionTracker = new ExecutionTracker(null,
        Arrays.asList("first", "second"),
        Arrays.asList(Path.make("/pipes/out1"), Path.make("/pipes/out2")),
        Path.make("/work"));

    Long id = repo.create(executionTracker);

    byte[] pipelineJsonEncoded = Files.readAllBytes(Paths.get(dbFolder.getPath()+ "/" + repo.getExecutionInfoFileName(id)));
    String pipelineJson = new String(pipelineJsonEncoded);
    String expectedPipelineJson = "{\"id\":" + id + "," +
        "\"chronosJobs\":[\"first\",\"second\"]," +
        "\"outputs\":[\"/pipes/out1\",\"/pipes/out2\"]," +
        "\"workDir\":\"/work\"}";
    assertThat(pipelineJson, is(expectedPipelineJson));
  }

  @Test
  public void canStorePipelineInADirectoryWhichAlreadyHasDirectoriesAndFiles() throws Exception
  {
    File alreadyInRepoFile = new File(dbFolder.getPath()+ "/randFile");
    assertThat(alreadyInRepoFile.createNewFile(), is(true));
    File alreadyInRepoDirectory = new File(dbFolder.getPath()+ "/randDir");
    assertThat(alreadyInRepoDirectory.mkdir(), is(true));

    ExecutionTracker executionTracker = new ExecutionTracker(null,
        Arrays.asList("first", "second"),
        Arrays.asList(Path.make("/pipes/out1"), Path.make("/pipes/out2")),
        Path.make("/work"));
    repo.create(executionTracker);
  }
}
