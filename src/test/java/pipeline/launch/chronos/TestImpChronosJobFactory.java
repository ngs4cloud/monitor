package pipeline.launch.chronos;

import org.junit.Before;
import org.junit.Test;
import pipeline.execution.Path;
import pipeline.execution.Task;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static junit.framework.TestCase.assertEquals;
import static java.util.stream.Collectors.*;

/**
 *
 */
public class TestImpChronosJobFactory {

  public static final String COMMAND = "mkdir /test";
  public static final String DOCKER_IMAGE = "test/image";
  public static final int MEM = 100;
  public static final int DISK = 110;
  public static final float CPUS = 0.5F;
  public static final String BASE_WORK_DIRECTORY = "/pipes";
  public static final String BASE_JOB_NAME = "test";
  public static final String JOBS_OWNER = "mail@example.com";
  public static final String RUN_ONCE_SCHEDULE = "R1//PT30M";
  public static final String EPSILON = "P1Y12M12D";
  public static final String CONTAINER_TYPE = "DOCKER";
  public static final String VOLUME_MODE = "RW";
  public static final String CHRONOS_HOST = "localhost";
  public static final int CHRONOS_PORT = 8080;
  public static final String WGET_DOCKER_IMAGE = "jnforja/wget";
  public static final String P7ZIP_DOCKER_IMAGE = "jnforja/7zip";


  private ImpChronosJobsFactory factory;
  private Task independentTask;
  private Task dependentTask;

  @Before
  public void setUp() throws Exception
  {
    this.factory = new ImpChronosJobsFactory(Path.make(BASE_WORK_DIRECTORY),
        BASE_JOB_NAME, JOBS_OWNER, CHRONOS_HOST, CHRONOS_PORT, WGET_DOCKER_IMAGE, P7ZIP_DOCKER_IMAGE);

    List<Integer> parents = Collections.emptyList();
    this.independentTask = new Task(1, DOCKER_IMAGE, COMMAND, MEM, DISK, CPUS, parents);
    this.dependentTask = new Task(2, DOCKER_IMAGE, COMMAND, MEM, DISK, CPUS, Arrays.asList(1));
  }

  @Test
  public void givenTasksDirectoriesAndNameOfInputFileCreatesExpectedChronosJobs() throws Exception
  {
    Stream<Task> tasks = Stream.of(independentTask, dependentTask);
    List<Path> dirsToCreate = Arrays.asList(Path.make("/pipes/test"));
    String inputFile = "input";

    List<ChronosJobDto> jobs = factory.getJobs(tasks, dirsToCreate.stream(), inputFile).collect(toList());

    final String commandExecutedToExtractInputAndCreateWorkingDirs =
        "cd " + BASE_WORK_DIRECTORY + " && 7z x " + inputFile + " && mkdir -p " + dirsToCreate.get(0);

    ChronosJobDto extractInputAndCreateWorkingDirsJob = jobs.get(0);
    final String expectedSetupJobName = BASE_JOB_NAME + "p7zip";
    assertEquals(expectedSetupJobName, extractInputAndCreateWorkingDirsJob.name);
    assertEquals(commandExecutedToExtractInputAndCreateWorkingDirs, extractInputAndCreateWorkingDirsJob.command);
    assertEquals(RUN_ONCE_SCHEDULE, extractInputAndCreateWorkingDirsJob.schedule);
    assertEquals(EPSILON, extractInputAndCreateWorkingDirsJob.epsilon);
    assertEquals(1000, extractInputAndCreateWorkingDirsJob.mem);
    assertEquals(500, extractInputAndCreateWorkingDirsJob.disk);
    assertEquals(1F, extractInputAndCreateWorkingDirsJob.cpus);
    assertEquals(null, extractInputAndCreateWorkingDirsJob.parents);
    ContainerDto container = extractInputAndCreateWorkingDirsJob.container;
    assertEquals(CONTAINER_TYPE, container.type);
    assertEquals(P7ZIP_DOCKER_IMAGE, container.image);
    assertEquals(BASE_WORK_DIRECTORY, container.volumes.get(0).containerPath);
    assertEquals(BASE_WORK_DIRECTORY, container.volumes.get(0).hostPath);
    assertEquals(VOLUME_MODE, container.volumes.get(0).mode);

    ChronosJobDto fromIndependentTaskJob = jobs.get(1);
    assertTaskProducedExpectedJob(independentTask, fromIndependentTaskJob, Arrays.asList(expectedSetupJobName));

    ChronosJobDto fromDependentTaskJob = jobs.get(2);
    assertTaskProducedExpectedJob(dependentTask, fromDependentTaskJob, Arrays.asList(BASE_JOB_NAME + independentTask.getId()));

    ChronosJobDto cleanUpJob = jobs.get(3);
    assertCleanUpJobIsExpected(cleanUpJob);
  }

  private void assertCleanUpJobIsExpected(ChronosJobDto cleanUpLauncherJob)
  {
    String command = "cd " + BASE_WORK_DIRECTORY  + " && chmod -R 777 ./ && wget " +
        CHRONOS_HOST + ":" + CHRONOS_PORT + "/scheduler/iso8601" +
        " --header \"Content-Type: application/json\"" +
        " --post-data \"{" +
        "\\\"name\\\": \\\""+BASE_JOB_NAME+"_RmWorkDirs\\\"," +
        "\\\"epsilon\\\": \\\"" + EPSILON + "\\\"," +
        "\\\"schedule\\\": \\\"R1/$(date -d \"+7 days\" +'%Y-%m-%dT00:00:00Z')/" + EPSILON + "\\\"," +
        "\\\"owner\\\": \\\"" + JOBS_OWNER + "\\\"," +
        "\\\"retries\\\": 2," +
        "\\\"command\\\": \\\"rm -rf " + BASE_WORK_DIRECTORY + "\\\"" +
        "}\"";

    assertEquals(BASE_JOB_NAME+"_changeAccessPermissionLaunchRmWorkDirs", cleanUpLauncherJob.name);
    assertEquals(command, cleanUpLauncherJob.command);
    assertEquals(null, cleanUpLauncherJob.schedule);
    assertEquals(EPSILON, cleanUpLauncherJob.epsilon);
    assertEquals(100, cleanUpLauncherJob.mem);
    assertEquals(100, cleanUpLauncherJob.disk);
    assertEquals(1F, cleanUpLauncherJob.cpus);
    List<String> expectedParents = Arrays.asList(BASE_JOB_NAME + 1, BASE_JOB_NAME + 2);
    assertEquals(expectedParents, cleanUpLauncherJob.parents);
    ContainerDto container = cleanUpLauncherJob.container;
    assertEquals(CONTAINER_TYPE, container.type);
    assertEquals(WGET_DOCKER_IMAGE, container.image);
    assertEquals(BASE_WORK_DIRECTORY, container.volumes.get(0).containerPath);
    assertEquals(BASE_WORK_DIRECTORY, container.volumes.get(0).hostPath);
    assertEquals(VOLUME_MODE, container.volumes.get(0).mode);
  }

  @Test
  public void givenTasksDirectoriesAndARemoteInputUriCreatesExpectedChronosJobs() throws Exception
  {
    Stream<Task> tasks = Stream.of(independentTask, dependentTask);
    List<Path> dirsToCreate = Arrays.asList(Path.make("/pipes/test"));
    URI remoteInput = new URI("http://testmonitor/remoteinput");

    List<ChronosJobDto> jobs = factory.getJobs(tasks, dirsToCreate.stream(), remoteInput).collect(toList());

    ChronosJobDto downloadRemoteInputJob = jobs.get(0);

    final String downloadRemoteInputCommand = "mkdir -p " + BASE_WORK_DIRECTORY + " && cd "
        + BASE_WORK_DIRECTORY + " && wget -O input " + remoteInput.toString();
    final String downloadRemoteInputJobName = BASE_JOB_NAME + "wget";
    assertEquals(downloadRemoteInputJobName, downloadRemoteInputJob.name);
    assertEquals(downloadRemoteInputCommand, downloadRemoteInputJob.command);
    assertEquals(RUN_ONCE_SCHEDULE, downloadRemoteInputJob.schedule);
    assertEquals(EPSILON, downloadRemoteInputJob.epsilon);
    assertEquals(100, downloadRemoteInputJob.mem);
    assertEquals(100, downloadRemoteInputJob.disk);
    assertEquals(1F, downloadRemoteInputJob.cpus);
    assertEquals(null, downloadRemoteInputJob.parents);
    ContainerDto container = downloadRemoteInputJob.container;
    assertEquals(CONTAINER_TYPE, container.type);
    assertEquals(WGET_DOCKER_IMAGE, container.image);
    assertEquals(BASE_WORK_DIRECTORY, container.volumes.get(0).containerPath);
    assertEquals(BASE_WORK_DIRECTORY, container.volumes.get(0).hostPath);
    assertEquals(VOLUME_MODE, container.volumes.get(0).mode);

    ChronosJobDto uncompressInputAndCreateWorkDirsJob = jobs.get(1);
    final String uncompressInputAndCreateWorkDirsJobCommand = "cd " + BASE_WORK_DIRECTORY +
        " && 7z x input && mkdir -p " + dirsToCreate.get(0);
    final String uncompressInputAndCreateWorkDirsJobname = BASE_JOB_NAME + "p7zip";
    assertEquals(uncompressInputAndCreateWorkDirsJobCommand, uncompressInputAndCreateWorkDirsJob.command);
    assertEquals(uncompressInputAndCreateWorkDirsJobname, uncompressInputAndCreateWorkDirsJob.name);
    assertEquals(null, uncompressInputAndCreateWorkDirsJob.schedule);
    assertEquals(EPSILON, uncompressInputAndCreateWorkDirsJob.epsilon);
    assertEquals(1000, uncompressInputAndCreateWorkDirsJob.mem);
    assertEquals(500, uncompressInputAndCreateWorkDirsJob.disk);
    assertEquals(1F, uncompressInputAndCreateWorkDirsJob.cpus);
    assertEquals(downloadRemoteInputJobName, uncompressInputAndCreateWorkDirsJob.parents.get(0));
    ContainerDto container1 = uncompressInputAndCreateWorkDirsJob.container;
    assertEquals(CONTAINER_TYPE, container1.type);
    assertEquals(P7ZIP_DOCKER_IMAGE, container1.image);
    assertEquals(BASE_WORK_DIRECTORY, container1.volumes.get(0).containerPath);
    assertEquals(BASE_WORK_DIRECTORY, container1.volumes.get(0).hostPath);
    assertEquals(VOLUME_MODE, container1.volumes.get(0).mode);

    ChronosJobDto fromIndependentTaskJob = jobs.get(2);
    assertTaskProducedExpectedJob(independentTask, fromIndependentTaskJob, Arrays.asList(uncompressInputAndCreateWorkDirsJobname));

    ChronosJobDto fromDependentTaskJob = jobs.get(3);
    assertTaskProducedExpectedJob(dependentTask, fromDependentTaskJob, Arrays.asList(BASE_JOB_NAME + independentTask.getId()));

    ChronosJobDto cleanUpJob = jobs.get(4);
    assertCleanUpJobIsExpected(cleanUpJob);
  }

  private void assertTaskProducedExpectedJob(Task taskUsedToProduceJob, ChronosJobDto job, List<String> expectedParents)
  {
    final String commandExecutedOnBaseWorkDirectory = "cd " + BASE_WORK_DIRECTORY + " && " + COMMAND;

    assertEquals(BASE_JOB_NAME + taskUsedToProduceJob.getId(), job.name);
    assertEquals(commandExecutedOnBaseWorkDirectory, job.command);
    assertEquals(null, job.schedule);
    assertEquals(EPSILON, job.epsilon);
    assertEquals(MEM, job.mem);
    assertEquals(DISK, job.disk);
    assertEquals(CPUS, job.cpus);
    assertEquals(expectedParents, job.parents);
    assertEquals(CONTAINER_TYPE, job.container.type);
    assertEquals(DOCKER_IMAGE, job.container.image);
    assertEquals(BASE_WORK_DIRECTORY, job.container.volumes.get(0).containerPath);
    assertEquals(BASE_WORK_DIRECTORY, job.container.volumes.get(0).hostPath);
    assertEquals(VOLUME_MODE, job.container.volumes.get(0).mode);
  }
}
