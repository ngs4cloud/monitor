package pipeline.launch.chronos;

import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.sshd.common.util.Pair;
import org.junit.Before;
import org.junit.Test;
import pipeline.execution.Path;
import pipeline.execution.Task;
import pipeline.http.HttpClient;

import java.net.URI;
import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class TestChronosJobLauncher {

  public static final String HOST = "test.com";
  public static final int PORT = 8080;

  private MockChronosJobFactory mockFactory;
  private MockSuccessHttpClient mockHttpClient;
  private ImpChronosJobsLauncher launcher;
  private Stream<Task> tasks;
  private Stream<Path> directories;

  @Before
  public void setUp() throws Exception
  {
    mockFactory = new MockChronosJobFactory();
    mockHttpClient = new MockSuccessHttpClient();
    launcher = new ImpChronosJobsLauncher(HOST, PORT, mockFactory, mockHttpClient);
    tasks = Stream.of(
        new Task(1, "test/image", "mkdir /test", 100, 110, 0.5F, Collections.emptyList()),
        new Task(2, "test/image", "echo \"test\" >> file.txt", 100, 110, 0.5F, Arrays.asList(2))
    );
    directories = Stream.of(
        Path.make("/dirOne"),
        Path.make("/dirTwo")
    );
  }

  @Test(expected = ImpChronosJobsLauncher.JobLaunchFailedException.class)
  public void whenAJobLaunchFailedThrowsJobLaunchFailedException() throws Exception
  {
    MockChronosJobFactory factory = new MockChronosJobFactory();
    ImpChronosJobsLauncher launcher =
        new ImpChronosJobsLauncher(HOST, PORT, factory, new MockFailureHttpClient());
    launcher.launch(Stream.<Task>empty(), Stream.<Path>empty(), "zipFile.zip");
  }

  @Test
  public void canLaunchJobsFromTasksDirectoriesAndInputZipFile() throws Exception
  {
    String input = "input.zip";

    List<String> jobNames = launcher.launch(tasks, directories, input).collect(toList());

    assertThat(mockFactory.givenTasks, is(tasks));
    assertThat(mockFactory.givenDirectories, is(directories));
    assertThat(mockFactory.givenInputName, is(input));

    assertJobsWereCorrectlyLaunched(jobNames);
  }

  @Test
  public void canLaunchJobsFromTasksDirectoriesAndRemoteInputFile() throws Exception
  {
    URI remoteInput =  new URI("http://testmonitor/input");

    List<String> jobNames = launcher.launch(tasks, directories, remoteInput).collect(toList());

    assertThat(mockFactory.givenTasks, is(tasks));
    assertThat(mockFactory.givenDirectories, is(directories));
    assertThat(mockFactory.givenRemoteInput, is(remoteInput));

    assertJobsWereCorrectlyLaunched(jobNames);
  }

  private void assertJobsWereCorrectlyLaunched(List<String> jobNames)
  {
    List<String> expectedJobNames = mockFactory.jobsReturned.stream().map(job -> job.name).collect(toList());
    assertThat(jobNames, is(expectedJobNames));

    String scheduledJobsEndpoint = mockHttpClient.argsGivenToPostAsJson.get(0).getFirst();
    ChronosJobDto scheduledJob = mockHttpClient.argsGivenToPostAsJson.get(0).getSecond();
    assertThat(scheduledJobsEndpoint, is("http://" + HOST + ":" + PORT + "/scheduler/iso8601"));
    assertThat(scheduledJob, is(mockFactory.jobsReturned.get(0)));

    String dependentJobsEndpoint = mockHttpClient.argsGivenToPostAsJson.get(1).getFirst();
    ChronosJobDto dependentJob = mockHttpClient.argsGivenToPostAsJson.get(1).getSecond();
    assertThat(dependentJobsEndpoint, is("http://" + HOST + ":" + PORT + "/scheduler/dependency"));
    assertThat(dependentJob, is(mockFactory.jobsReturned.get(1)));
  }

  private static class MockChronosJobFactory implements ChronosJobsFactory{

    public List<ChronosJobDto> jobsReturned;
    public Stream<Task> givenTasks;
    public Stream<Path> givenDirectories;
    public String givenInputName;
    public URI givenRemoteInput;

    @Override
    public Stream<ChronosJobDto> getJobs(Stream<Task> tasks, Stream<Path> directories, URI remoteInput)
    {
      this.givenTasks = tasks;
      this.givenDirectories = directories;
      this.givenRemoteInput = remoteInput;
      this.jobsReturned = Arrays.asList(
          getStubScheduledJobWithName("first"),
          getStubDependentJobWithName("second")
      );
      return this.jobsReturned.stream();
    }

    @Override
    public Stream<ChronosJobDto> getJobs(Stream<Task> tasks, Stream<Path> directories, String inputName)
    {
      this.givenTasks = tasks;
      this.givenDirectories = directories;
      this.givenInputName = inputName;
      this.jobsReturned = Arrays.asList(
          getStubScheduledJobWithName("first"),
          getStubDependentJobWithName("second")
      );
      return this.jobsReturned.stream();
    }

    private static ChronosJobDto getStubScheduledJobWithName(String name)
    {
      return new ChronosJobDto(name, null, 0, 0, 0, null, null, null, "R1//PT30M", null);
    }

    private static ChronosJobDto getStubDependentJobWithName(String name)
    {
      return new ChronosJobDto(name, null, 0, 0, 0, null, null, null, null, Arrays.asList("first"));
    }
  }

  private static class MockSuccessHttpClient implements HttpClient {

    private final List<Pair<String,ChronosJobDto>> argsGivenToPostAsJson = new ArrayList<>();

    @Override
    public HttpResponse<String> postAsJson(String url, Object body)
    {
      this.argsGivenToPostAsJson.add(new Pair<>(url,(ChronosJobDto)body));
      return new MockHttpResponse<>(204);
    }

    @Override
    public <T> HttpResponse<T> getAsJson(String url, TypeReference<T> type)
    {
      return null;
    }
  }

  private static class MockFailureHttpClient implements HttpClient{

    @Override
    public HttpResponse<String> postAsJson(String url, Object body)
    {
      return new MockHttpResponse<>(500);
    }

    @Override
    public <T> HttpResponse<T> getAsJson(String url, TypeReference<T> type)
    {
      return null;
    }
  }

  private static class MockHttpResponse<T> implements HttpClient.HttpResponse<T>{

    private final int status;

    public MockHttpResponse(int status)
    {
      this.status = status;
    }

    @Override
    public int getStatus()
    {
      return status;
    }

    @Override
    public String getStatusText()
    {
      return null;
    }

    @Override
    public Map<String, List<String>> getHeaders()
    {
      return null;
    }

    @Override
    public T getBody()
    {
      return null;
    }
  }
}
