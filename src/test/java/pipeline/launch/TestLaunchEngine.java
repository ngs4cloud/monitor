package pipeline.launch;

import org.junit.Test;
import pipeline.execution.*;
import pipeline.launch.chronos.ChronosJobsLauncher;
import pipeline.repo.ExecutionTracker;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static java.util.stream.Collectors.*;

/**
 *
 */
public class TestLaunchEngine {

  @Test
  public void canLaunchPipelineFromInputStreamOfAnIRWhenInputIsLocal() throws Exception
  {
    final URI input = new URI("file:///C:/Users/Dio/input.zip");
    MockExecutionParser irParser = new MockExecutionParser(input);
    MockClusterInitializer initializer = new MockClusterInitializer();
    MockChronosJobsLauncher jobsLauncher = new MockChronosJobsLauncher();
    MockExecutionTrackerRepo repo = new MockExecutionTrackerRepo();
    InputStream mockInputStream = new ByteArrayInputStream("mockIRJson".getBytes());
    Path workDir = Path.make("/make");
    LaunchEngine engine = new LaunchEngine(irParser::parse,
        initializer::init,
        jobsLauncher,
        repo::create,
        workDir);

    long id = engine.run(mockInputStream);

    assertThat(irParser.givenInputStream, is(mockInputStream));

    assertThat(initializer.givenFile, is(new File(input)));
    assertThat(initializer.givenDirectory, is(workDir));

    assertThat(irParser.returnedExecution.getDirectories(), is(jobsLauncher.givenDirectories.collect(toList())));
    assertThat(irParser.returnedExecution.getTasks(), is(jobsLauncher.givenTasks.collect(toList())));
    String expectedInputFileName = new File(irParser.returnedExecution.getInput()).getName();
    assertThat(expectedInputFileName, is(jobsLauncher.givenInputName));
    
    assertThat(repo.givenExecutionTracker.getJobNames(), is(jobsLauncher.returnedJobNames));
    assertThat(repo.givenExecutionTracker.getOutputs(), is(irParser.returnedExecution.getOutputs()));
    assertThat(repo.givenExecutionTracker.getWorkDir(), is(workDir));
    assertThat(id, is(repo.returnedExecutionIfId));
  }

  @Test
  public void canLaunchPipelineFromInputStreamOfAnIRWhenInputIsRemote() throws Exception
  {
    MockExecutionParser irParser = new MockExecutionParser(new URI("http://testmonitor/remoteinput"));
    MockClusterInitializer initializer = new MockClusterInitializer();
    MockChronosJobsLauncher jobsLauncher = new MockChronosJobsLauncher();
    MockExecutionTrackerRepo repo = new MockExecutionTrackerRepo();
    InputStream mockInputStream = new ByteArrayInputStream("mockIRJson".getBytes());
    Path workDir = Path.make("/make");
    LaunchEngine engine = new LaunchEngine(irParser::parse,
        initializer::init,
        jobsLauncher,
        repo::create,
        workDir);

    long id = engine.run(mockInputStream);

    assertThat(irParser.givenInputStream, is(mockInputStream));

    List<Path> expectedDirectories = Stream.concat(
        Stream.of(workDir),
        irParser.returnedExecution.getDirectories().stream()
    ).collect(toList());
    assertThat(expectedDirectories, is(jobsLauncher.givenDirectories.collect(toList())));
    assertThat(irParser.returnedExecution.getTasks(), is(jobsLauncher.givenTasks.collect(toList())));
    assertThat(irParser.returnedExecution.getInput(), is(jobsLauncher.givenRemoteInput));

    assertThat(repo.givenExecutionTracker.getJobNames(), is(jobsLauncher.returnedJobNames));
    assertThat(repo.givenExecutionTracker.getOutputs(), is(irParser.returnedExecution.getOutputs()));
    assertThat(repo.givenExecutionTracker.getWorkDir(), is(workDir));
    assertThat(id, is(repo.returnedExecutionIfId));
  }


  private static class MockExecutionParser {
    public final URI inputToBeUsed;
    public InputStream givenInputStream;
    private Execution returnedExecution;

    private MockExecutionParser(URI inputToBeUsed)
    {
      this.inputToBeUsed = inputToBeUsed;
    }

    public Execution parse(InputStream irStream)
    {
      givenInputStream = irStream;
        returnedExecution = new Execution(Arrays.asList(Path.make("/pipes")),
            inputToBeUsed,
            Arrays.asList(new Task(1,
                    "repo/samtools",
                    "samtools view -bS rel/eg2.sam > rel/eg2.bam",
                    128,
                    256,
                    1,
                    Collections.<Integer>emptyList())
            ),
            Arrays.asList(Path.make("/rel/eg2.sorted")));
        return returnedExecution;
    }
  }

  private static class MockClusterInitializer{
    public File givenFile;
    public Path givenDirectory;
    public void init(File inputs, Path workDirectory)
    {
      this.givenFile = inputs;
      this.givenDirectory = workDirectory;
    }
  }

  private static class MockChronosJobsLauncher implements ChronosJobsLauncher{
    public List<String> returnedJobNames;
    public Stream<Task> givenTasks;
    public Stream<Path> givenDirectories;
    public String givenInputName;
    public URI givenRemoteInput;

    @Override
    public Stream<String> launch(Stream<Task> tasks, Stream<Path> directories, URI remoteInput)
    {
      this.givenTasks = tasks;
      this.givenDirectories = directories;
      this.givenRemoteInput = remoteInput;
      this.returnedJobNames = Arrays.asList("name1");
      return this.returnedJobNames.stream();
    }

    @Override
    public Stream<String> launch(Stream<Task> tasks, Stream<Path> directories, String inputName)
    {
      this.givenTasks = tasks;
      this.givenDirectories = directories;
      this.givenInputName = inputName;
      this.returnedJobNames = Arrays.asList("name1");
      return this.returnedJobNames.stream();
    }
  }

  private static class MockExecutionTrackerRepo {
    public ExecutionTracker givenExecutionTracker;
    public long returnedExecutionIfId;

    public long create(ExecutionTracker executionTracker)
    {
      this.givenExecutionTracker = executionTracker;
      this.returnedExecutionIfId = 1L;
      return this.returnedExecutionIfId;
    }
  }
}
