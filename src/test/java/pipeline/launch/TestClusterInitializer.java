package pipeline.launch;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import pipeline.execution.Path;
import pipeline.output.Output;
import pipeline.ssh.SftpClient;

import java.io.File;
import java.util.function.Consumer;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class TestClusterInitializer {

  @Rule
  public TemporaryFolder tempFolder = new TemporaryFolder();

  private static final String BASE_WORK_DIR = "/pipes";

  private MockSftpClient mockSftpClient;
  private Path baseWorkDirPath;
  private ClusterInitializer clusterInitializer;

  @Before
  public void setUp() throws Exception
  {
    mockSftpClient = new MockSftpClient();
    baseWorkDirPath = Path.make(BASE_WORK_DIR);
    clusterInitializer = new ClusterInitializer(mockSftpClient);
  }

  @Test
  public void canCreateBaseWorkDirectoryAndUploadInputFileToCluster() throws Exception
  {
    File input = tempFolder.newFile("MockInputFile");
    clusterInitializer.initCluster(input, baseWorkDirPath);

    LocalInput givenInput = mockSftpClient.mockChannel.inputGivenToPut;
    assertThat(mockSftpClient.mockChannel.dirPathPassedToMkDir, is(baseWorkDirPath));
    assertThat(givenInput.getSrc(), is(input));
    assertThat(givenInput.getDest(), is(baseWorkDirPath.concat(Path.make(input.getName()))));
  }

  public static class MockSftpClient implements SftpClient {

    public MockSftpChannel mockChannel = new MockSftpChannel();

    @Override
    public void start(Consumer<SftpChannel> actions)
    {
      actions.accept(mockChannel);
    }

    public static class MockSftpChannel implements SftpClient.SftpChannel{

      public Path dirPathPassedToMkDir;
      public LocalInput inputGivenToPut;

      @Override
      public void put(LocalInput input)
      {
        inputGivenToPut = input;
      }

      @Override
      public void get(Output output)
      {

      }

      @Override
      public void rm(Path path)
      {

      }

      @Override
      public void mkdir(Path path)
      {
        dirPathPassedToMkDir = path;
      }
    }
  }
}
