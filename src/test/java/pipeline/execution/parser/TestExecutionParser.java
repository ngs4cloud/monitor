package pipeline.execution.parser;

import org.junit.Test;
import pipeline.execution.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class TestExecutionParser {

  @Test(expected = ExecutionParser.DoesNotFollowIRSchemaException.class)
  public void givenInputStreamOfJsonThatDoNotCorrespondToIRJsonSchemaThrowsException() throws Exception
  {
    String invalidIRJson = "{\"x\":1}";
    ByteArrayInputStream is = new ByteArrayInputStream(invalidIRJson.getBytes());
    new ExecutionParser().parse(is);
  }

  @Test
  public void givenValidJsonBuildsExpectedIR() throws Exception
  {
    FileInputStream irJsonIs = new FileInputStream(new File(ClassLoader.getSystemResource("pipeline/execution/IR.json").toURI()));
    Execution execution = new ExecutionParser().parse(irJsonIs);

    List<Path> expectedDirectories = Arrays.asList(Path.make("/res"));
    List<Task> expectedTasks = Arrays.asList(new Task(1,
            "repo/samtools",
            "samtools view -bS rel/eg2.sam > rel/eg2.bam",
            128,
            256,
            1,
            Collections.<Integer>emptyList())
    );
    List<Path> expectedOutputs = Arrays.asList(Path.make("/rel/eg2.sorted"));
    URI expectedInput = new URI("file:///C:/Users/Dio/input.zip");
    Execution expectedExecution = new Execution(expectedDirectories, expectedInput, expectedTasks, expectedOutputs);
    assertThat(execution, is(expectedExecution));
  }
}
