package pipeline.execution;

import org.junit.Test;

import java.util.Arrays;

import static junit.framework.TestCase.*;

/**
 *
 */
public class TestPath {

  @Test(expected = Path.InvalidPathException.class)
  public void whenGivenAnEmptyPathInvalidPathExceptionIsThrown() throws Exception
  {
    Path.make("");
  }

  @Test
  public void canMakePathWhichIsAbsoluteFromAbsolutePathInString() throws Exception
  {
    String absoluteStrPath = "/pipes";
    Path absolutePath = Path.make(absoluteStrPath);
    assertTrue(absolutePath.isAbsolute());
  }

  @Test
  public void canMakePathWhichIsRelativeFromRelativePathInString() throws Exception
  {
    String relativeStrPath = "pipes";
    Path relativePath = Path.make(relativeStrPath);
    assertFalse(relativePath.isAbsolute());
  }

  @Test
  public void whenGivenARelativePathWhichStartsWithADotItsStringRepresentationDoesNotHaveTheDot() throws Exception
  {
    String relativeStrPathWithDot = "./pipes";
    Path relativePath = Path.make(relativeStrPathWithDot);
    assertEquals("pipes", relativePath.toString());
  }

  @Test
  public void givenAbsolutePathInStringWithDepthOf2GetDepthReturns2()
  {
    String pathStrWith2OfDepth = "/pipes/first/second";
    Path pathWith2OfDepth = Path.make(pathStrWith2OfDepth);
    assertEquals(2, pathWith2OfDepth.getDepth());
  }

  @Test
  public void givenRelativePathInStringWithDepthOf3GetDepthReturns3()
  {
    String pathStrWith3OfDepth = "pipes/first/second/third";
    Path pathWith3OfDepth = Path.make(pathStrWith3OfDepth);
    assertEquals(3, pathWith3OfDepth.getDepth());
  }

  @Test
  public void testConcatOfAnAbsolutePathWithRelativePathResultsInAbsolutePath() throws Exception
  {
    final boolean absolute = true;
    Path absolutePath = new Path(Arrays.asList("pipes"), absolute);
    final boolean relative = false;
    Path relativePathToConcatWith = new Path(Arrays.asList("example"), relative);

    Path concatAbsolutePath = absolutePath.concat(relativePathToConcatWith);
    assertTrue(absolutePath.isAbsolute());
    assertEquals("/pipes/example", concatAbsolutePath.toString());
  }

  @Test
  public void testConcatOfARelativePathWithAnAbsolutePathResultsInRelativePath() throws Exception
  {
    final boolean relative = false;
    Path relativePath = new Path(Arrays.asList("pipes"), relative);
    final boolean absolute = true;
    Path relativePathToConcatWith = new Path(Arrays.asList("example"), absolute);

    Path concatAbsolutePath = relativePath.concat(relativePathToConcatWith);
    assertFalse(relativePath.isAbsolute());
    assertEquals("pipes/example", concatAbsolutePath.toString());
  }

  @Test
  public void testCanGetLastSegmentOfPath() throws Exception
  {
    Path path = Path.make("First/Second/Last");
    assertEquals("Last", path.getLastSegment().toString());
  }
}
