package pipeline.output;

import org.junit.Test;
import pipeline.execution.Path;
import pipeline.launch.LocalInput;
import pipeline.ssh.SftpClient;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class TestOutputDownloader {

  @Test
  public void canDownloadOutput() throws Exception
  {
    MockSftpChannel mockChannel = new MockSftpChannel();
    MockSftpClient mockClient = new MockSftpClient(mockChannel);
    Path baseDir = Path.make("/base");
    OutputsDownloader outputsDownloader = new OutputsDownloader(mockClient, baseDir);


    final ByteArrayOutputStream dest = new ByteArrayOutputStream();
    List<Output> outputs = Arrays.asList(
      new Output(dest, Path.make("/pipes/first")),
      new Output(dest, Path.make("/pipes/second")),
      new Output(dest, Path.make("/pipes/third"))
    );

    List<Output> expectedOutputs = Arrays.asList(
        new Output(dest, baseDir.concat(Path.make("/pipes/first"))),
        new Output(dest, baseDir.concat(Path.make("/pipes/second"))),
        new Output(dest, baseDir.concat(Path.make("/pipes/third")))
    );

    outputsDownloader.download(outputs.stream());

    assertThat(mockChannel.outputsGivenToGet, is(expectedOutputs));
  }

    public static class MockSftpClient implements SftpClient{

      public final MockSftpChannel mockSftpChannel;

      public MockSftpClient(MockSftpChannel mockSftpChannel)
      {
        this.mockSftpChannel = mockSftpChannel;
      }

      @Override
      public void start(Consumer<SftpChannel> actions)
      {
        actions.accept(mockSftpChannel);
      }
    }

    public static class MockSftpChannel implements SftpClient.SftpChannel{

      public final List<Output> outputsGivenToGet = new ArrayList<>();

      @Override
      public void put(LocalInput input)
      {

      }

      @Override
      public void get(Output output)
      {
        outputsGivenToGet.add(output);
      }

      @Override
      public void rm(Path path)
      {

      }

      @Override
      public void mkdir(Path path)
      {

      }
  }
}
