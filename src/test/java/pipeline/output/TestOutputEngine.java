package pipeline.output;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import pipeline.execution.Path;
import pipeline.status.ExecutionStatus;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class TestOutputEngine {

  @Rule
  public TemporaryFolder tempFolder = new TemporaryFolder();

  private MockExecutionStatusGetter statusGetter;
  private MockOutputGetter outputGetter;
  private MockOutputDownloader outputDownloader;

  @Before
  public void setUp() throws Exception
  {
    this.statusGetter = new MockExecutionStatusGetter();
    this.outputGetter = new MockOutputGetter();
    this.outputDownloader = new MockOutputDownloader();
  }

  @Test(expected = OutputEngine.PipelineExecutionHasFailedException.class)
  public void whenTheExecutionHasFailedThrowsPipelineExecutionHasFailedException() throws Exception
  {
    OutputEngine engine = new OutputEngine(statusGetter::getFailedExecution,
        outputGetter::getOutputs,
        outputDownloader::download);

    engine.run(Path.make("/destDir"));
  }

  @Test(expected = OutputEngine.PipelineExecutionStillInProgressException.class)
  public void whenTheExecutionIsInProgressThrowsPipelineExecutionStillInProgressException() throws Exception
  {
    OutputEngine engine = new OutputEngine(statusGetter::getInProgressExecution,
        outputGetter::getOutputs,
        outputDownloader::download);

    engine.run(Path.make("/destDir"));
  }

  @Test
  public void whenTheExecutionHasFinishedDownloadsAllTheOutputs() throws Exception
  {
    OutputEngine engine = new OutputEngine(statusGetter::getFinishedExecution,
        outputGetter::getOutputs,
        outputDownloader::download);

    engine.run(Path.make(tempFolder.newFolder().getPath()));

    List<Output> actualOutputs = outputDownloader.givenOutputs;
    List<Path> outputPaths = outputGetter.getOutputs().collect(Collectors.toList());
    assertThat(actualOutputs.get(0).getSrc(), is(outputPaths.get(0)));
    assertThat(actualOutputs.get(1).getSrc(), is(outputPaths.get(1)));
  }

  private static class MockExecutionStatusGetter {

    public ExecutionStatus getFailedExecution(){
      return ExecutionStatus.makeFailed("Failed");
    }

    public ExecutionStatus getFinishedExecution(){
      return ExecutionStatus.makeFinished("Finished");
    }

    public ExecutionStatus getInProgressExecution(){
      return ExecutionStatus.makeInProgress("In progress");
    }
  }

  private static class MockOutputGetter {

    public Stream<Path> getOutputs()
    {
      return Stream.of(
          Path.make("/out/res1"),
          Path.make("/out/res2"));
    }
  }

  private static class MockOutputDownloader{

    public List<Output> givenOutputs;

    public void download(Stream<Output> outputs)
    {
      this.givenOutputs = outputs.collect(Collectors.toList());
    }
  }
}
