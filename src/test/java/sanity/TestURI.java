package sanity;

import org.junit.Test;

import java.net.URI;
import static junit.framework.TestCase.assertEquals;

/**
 *
 */
public class TestURI {

  @Test
  public void testToStringReturnsStringUsedInCreationOfURI() throws Exception
  {
    final String uriStr = "http://example.com";
    URI uri = new URI(uriStr);
    assertEquals(uriStr, uri.toString());
  }
}
