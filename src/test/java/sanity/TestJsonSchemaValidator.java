package sanity;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import org.junit.Test;


import java.io.File;

import static junit.framework.TestCase.assertTrue;


/**
 *
 */
public class TestJsonSchemaValidator {

  public static final String JSON_V4_SCHEMA_IDENTIFIER = "http://json-schema.org/draft-04/schema#";
  public static final String JSON_SCHEMA_IDENTIFIER_ELEMENT = "$schema";

  @Test
  public void testJsonSchemaValidator() throws Exception
  {
    File schemaFile = new File(ClassLoader.getSystemResource("testSchema.json").toURI());
    File jsonFile = new File(ClassLoader.getSystemResource("data.json").toURI());

    final JsonNode schemaNode = JsonLoader.fromFile(schemaFile);
    final JsonNode schemaIdentifier = schemaNode.get(JSON_SCHEMA_IDENTIFIER_ELEMENT);
    if (null == schemaIdentifier){
      ((ObjectNode) schemaNode).put(JSON_SCHEMA_IDENTIFIER_ELEMENT, JSON_V4_SCHEMA_IDENTIFIER);
    }
    final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
    JsonSchema jsonSchema = factory.getJsonSchema(schemaNode);
    JsonNode dataNode = JsonLoader.fromFile(jsonFile);
    ProcessingReport report = jsonSchema.validate(dataNode);
    assertTrue(report.isSuccess());
  }
}
