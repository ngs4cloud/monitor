package sanity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 */
public class TestJackson {

  @Test
  public void canConvertJavaObjectToJSON() throws Exception
  {
    ObjectMapper mapper = new ObjectMapper();
    Point p = new Point(1,2);

    String jsonPoint = mapper.writeValueAsString(p);
    assertThat(jsonPoint, is("{\"x\":1,\"y\":2}"));
  }

  @Test
  public void ignoresNullFieldsWhenConvertingJavaObjectToJson() throws Exception
  {
    ObjectMapper mapper = new ObjectMapper();
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    NamedPoint np = new NamedPoint(null, 1, 2);

    String namedJsonPoint = mapper.writeValueAsString(np);
    assertThat(namedJsonPoint, is("{\"x\":1,\"y\":2}"));
  }

  @Test
  public void listIsConvertedToJSONArray() throws Exception
  {
    ObjectMapper mapper = new ObjectMapper();
    List<Integer> ints = Arrays.asList(1, 2, 3, 4);

    String intsJson = mapper.writeValueAsString(ints);
    assertThat(intsJson, is("[1,2,3,4]"));
  }

  @Test
  public void convertsJsonToPoint() throws Exception
  {
    String jsonPoint = "{\"x\":1,\"y\":2}";
    ObjectMapper mapper = new ObjectMapper();

    Point point = mapper.readValue(jsonPoint, Point.class);
    assertThat(point.x, is(1));
    assertThat(point.y, is(2));
  }

  @Test
  public void convertsJsonToNamedPoint() throws Exception
  {
    String jsonNamedPoint = "{\"x\":1,\"y\":2}";
    ObjectMapper mapper = new ObjectMapper();

    NamedPoint namedPoint = mapper.readValue(jsonNamedPoint, NamedPoint.class);

    assertThat(namedPoint.name, is((String)null));
    assertThat(namedPoint.x, is(1));
    assertThat(namedPoint.y, is(2));
  }

  @Test
  public void canConvertJsonToObjectWhenObjectHasInnerClassAsField() throws Exception
  {
    String jsonColorPoint = "{\"x\":1,\"y\":2,\"color\":{\"color\":\"green\"}}";
    ObjectMapper mapper = new ObjectMapper();

    ColorPoint colorPoint = mapper.readValue(jsonColorPoint, ColorPoint.class);
    assertThat(colorPoint.x, is(1));
    assertThat(colorPoint.y, is(2));
    assertThat(colorPoint.color.color, is("green"));
  }

  @Test
  public void canCovertJsonArrayOfPointToListOfPoints() throws Exception
  {
    String jsonArrayPoints = "[{\"x\":1 ,\"y\":1},{\"x\":2 ,\"y\":2}]";

    TypeReference<List<Point>> type = new TypeReference<List<Point>>(){};
    List<Point> points = new ObjectMapper().readValue(jsonArrayPoints, type);

    List<Point> expectedPoints = Arrays.asList(new Point(1,1), new Point(2,2));
    assertThat(points, is(expectedPoints));
  }

  private static class Point{
    public final int x;
    public final int y;

    @JsonCreator
    public Point(@JsonProperty("x")int x,
                 @JsonProperty("y")int y)
    {
      this.x = x;
      this.y = y;
    }

    @Override
    public boolean equals(Object o)
    {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      Point point = (Point) o;

      if (x != point.x) return false;
      if (y != point.y) return false;

      return true;
    }

    @Override
    public int hashCode()
    {
      int result = x;
      result = 31 * result + y;
      return result;
    }
  }

  private static class NamedPoint{
    public final String name;
    public final int x;
    public final int y;

    @JsonCreator
    public NamedPoint(@JsonProperty("name") String name,
                      @JsonProperty("x") int x,
                      @JsonProperty("y") int y)
    {
      this.name = name;
      this.x = x;
      this.y = y;
    }
  }

  private static class ColorPoint{
    public final int x;
    public final int y;
    public final Color color;

    @JsonCreator
    public ColorPoint(@JsonProperty("x") int x,
                      @JsonProperty("y") int y,
                      @JsonProperty("color") Color color)
    {
      this.x = x;
      this.y = y;
      this.color = color;
    }

    private static class Color{
      public final String color;

      @JsonCreator
      public Color(@JsonProperty("color")String color)
      {
        this.color = color;
      }
    }
  }
}
