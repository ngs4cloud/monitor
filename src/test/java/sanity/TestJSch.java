package sanity;

import com.jcraft.jsch.*;
import org.apache.sshd.common.NamedFactory;
import org.apache.sshd.server.Command;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.auth.*;
import org.apache.sshd.server.auth.UserAuth;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.scp.ScpCommandFactory;
import org.apache.sshd.server.subsystem.sftp.SftpSubsystemFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static junit.framework.TestCase.*;

/**
 *
 */
public class TestJSch {

  public static final int PORT = 9000;

  private SshServer sshd;

  @Before
  public void setUp() throws Exception
  {
    sshd = SshServer.setUpDefaultServer();
    sshd.setPort(PORT);
    sshd.setKeyPairProvider(new SimpleGeneratorHostKeyProvider());

    List<NamedFactory<UserAuth>> userAuthFactories = new ArrayList<>();
    userAuthFactories.add(new UserAuthNoneFactory());

    sshd.setUserAuthFactories(userAuthFactories);
    sshd.setPublickeyAuthenticator((s, publicKey, serverSession) -> true);

    sshd.setCommandFactory(new ScpCommandFactory());

    List<NamedFactory<Command>> namedFactoryList = new ArrayList<>();
    namedFactoryList.add(new SftpSubsystemFactory());
    sshd.setSubsystemFactories(namedFactoryList);

    sshd.start();
  }

  @After
  public void cleanUp() throws Exception
  {
    sshd.stop();
  }

  @Test
  public void CanUploadStringAsFileToServerAndDownloadItFromServer() throws Exception
  {
    JSch jsch = new JSch();
    Session session = jsch.getSession("user", "localhost", PORT);

    Properties config = new Properties();
    config.put("StrictHostKeyChecking", "no");
    session.setConfig(config);
    session.connect();

    Channel channel = session.openChannel("sftp");
    ChannelSftp sftp = (ChannelSftp) channel;
    sftp.connect();

    final String fileContent = "Test file content";
    final String updloadDest = "testFile";
    sftp.put(new ByteArrayInputStream(fileContent.getBytes()), updloadDest);

    ByteArrayOutputStream downloadedFile = new ByteArrayOutputStream(fileContent.getBytes().length);
    sftp.get(updloadDest, downloadedFile);
    assertEquals(fileContent, downloadedFile.toString());

    sftp.rm(updloadDest);
    sftp.disconnect();
    session.disconnect();
  }
}
