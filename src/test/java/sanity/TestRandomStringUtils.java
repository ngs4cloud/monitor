package sanity;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.util.HashSet;

/**
 *
 */
public class TestRandomStringUtils {

  @Test
  public void testThereIsNoCollisionIn1000000Runs() throws Exception
  {
    final int nRandomStrs = 1000000;
    HashSet<String> set = new HashSet<>(nRandomStrs);

    for (int i = 0; i < nRandomStrs ; i++) {
      final String s = RandomStringUtils.randomAlphabetic(12);
      if(set.contains(s)){
        throw new RuntimeException("Repeated string was generated");
      }
      set.add(s);
    }
  }
}
