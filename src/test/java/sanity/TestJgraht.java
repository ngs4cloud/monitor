package sanity;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.TopologicalOrderIterator;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 *
 */
public class TestJgraht {

  @Test
  public void testTopologicalSort() throws Exception
  {
    String first = "First";
    String second = "Second";
    String third = "Third";
    List<String> expectedOrder = Arrays.asList(first, second, third);

    DefaultDirectedGraph<String, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);
    graph.addVertex(second);
    graph.addVertex(third);
    graph.addVertex(first);

    graph.addEdge(first, second);
    graph.addEdge(second, third);

    TopologicalOrderIterator<String, DefaultEdge> orderIterator = new TopologicalOrderIterator<>(graph);
    int i = 0;
    while(orderIterator.hasNext()){
      assertEquals(expectedOrder.get(i++), orderIterator.next());
    }
  }

}
